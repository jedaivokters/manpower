var msgprompt = jQuery('<div class="easyui-window" style="min-width:300px;padding:20px" title="Message" />');
jQuery('body').append(msgprompt);
jQuery('.easyui-window').window({
    closed: true,
    modal: true,
    minimizable: false,
    maximizable: false,
    resizable: false,
    collapsible: false,
    closable: true
});
function validate_form(form) {
	var errCtr = 0;
	form.find('.validate').each(function(i,o){
		if( jQuery(o).attr('required') && jQuery(o).val() == '' ) {
				jQuery(o).focus().addClass('error');
			errCtr += 1;
		}
	});
	
	if( errCtr > 0 ) {
		return false;
	}
	return true;
}

$(function(){
    $('#topnav_inner > ul.sf-menu').superfish();
	
	
	jQuery('.apply').on('click',function(e){
		e.preventDefault();
		jQuery('.easyui-window').window('setTitle', 'Message');
		jQuery('.easyui-window').window('center');
		jQuery.ajax({
			url: jQuery(this).attr('href'),
			dataType: 'json',
			success: function(response) {
				if(response.message != '') {
					msgprompt.html(response.message);
					msgprompt.window('open');
				}
			}
		});
	});
});
