<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(FUEL_PATH.'models/base_module_model.php');

class News_model extends Base_module_model {
    public $foreign_keys = array('creator_id' => 'fuel_users_model',
    'news_type_id' => 'news_types_model',
    );
    public $hidden_fields = array('creator_id');
    public $required = array(
        'title' => 'Title is required.',
        'content' => 'Content is required.',
    );

    function __construct()
    {
        parent::__construct('news');
    }

    function form_fields($values = array())
    {
        $fields = parent::form_fields($values);

        // ******************* ADD CUSTOM FORM STUFF HERE *******************
        $fields['image']['folder'] = 'images';
        $fields['image']['required'];
        $fields['image']['comment'] = 'Image must be 50x50 in dimension.';
        $fields['creator_id']['label'] = 'Creator';
        $fields['news_type_id']['label'] = 'News type';
        return $fields;
    }

    function on_before_save($values)
    {
        $CI =& get_instance();
        $values['creator_id'] = $CI->fuel->auth->user_data('id');

        return $values;
    }

}

class New_model extends Base_module_record {

}
