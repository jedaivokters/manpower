<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(FUEL_PATH.'models/base_module_model.php');

class Social_tools_model extends Base_module_model {
    public $required = array(
        'image' => 'Image is required.',
    );

    function __construct()
    {
        parent::__construct('social_tools');
    }

    function form_fields($values = array())
    {
        $fields = parent::form_fields($values);

        // ******************* ADD CUSTOM FORM STUFF HERE *******************
        $fields['image']['folder'] = 'images';
        $fields['image']['required'];
        $fields['image']['comment'] = 'Image must be 20x20 in dimension.';

        return $fields;
    }


}

class Social_tools extends Base_module_record {

}
