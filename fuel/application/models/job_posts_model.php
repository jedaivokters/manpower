<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(FUEL_PATH.'models/base_module_model.php');

class Job_posts_model extends Base_module_model {
    public $foreign_keys = array('creator_id' => 'fuel_users_model',
        'occupation_type_id' => 'occupation_types_model',
        'position_type_id' => 'positions_model'
        );
    public $hidden_fields = array('creator_id');
    public $required = array(
        'title' => 'Job title is required.',
        'contact_no' => 'Contact no is required.',
        'address' => 'Address is required.',
        'city' => 'City no is required.',
        'zipcode' => 'Zipcode no is required.',
        'occupation_type_id' => 'Occupation Type is required.',
        'position_type_id' => 'Position type is required.',
    );

    function __construct()
    {
        parent::__construct('job_posts');
    }

    function form_fields($values = array())
    {
        $fields = parent::form_fields($values);

        // ******************* ADD CUSTOM FORM STUFF HERE *******************
        $fields['creator_id']['label'] = 'Creator';
        $fields['content']['label'] = 'Job Description';
        $fields['title'] = array('type' => 'textarea', 'cols' => 10, 'rows' => 1, 'class' => 'no_editor');
        $fields['contact_no']['label'] = 'Contact no.';
        $fields['occupation_type_id']['label'] = 'Occupation type';
        $fields['position_type_id']['label'] = 'Industry type';
        $fields['from_date']['label'] = 'From Date (Available)';
        $fields['to_date']['label'] = 'To Date (Available)';

        return $fields;
    }
	function list_items($limit = NULL, $offset = NULL, $col = 'id', $order = 'asc')
    {
        //$this->db->join('fuel_users', 'projects.manager_id = fuel_users.id', 'left');
        $this->db->select('job_posts.id, job_posts.title, job_posts.address, job_posts.city, job_posts.zipcode, job_posts.contact_no, job_posts.salary, job_posts.published', FALSE);
        $data = parent::list_items($limit, $offset, $col, $order);
        return $data;
    }
    function on_before_save($values)
    {
        $CI =& get_instance();
        $values['creator_id'] = $CI->fuel->auth->user_data('id');

        return $values;
    }

}

class Job_post_model extends Base_module_record {

}
