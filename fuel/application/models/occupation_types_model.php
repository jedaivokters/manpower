<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(FUEL_PATH.'models/base_module_model.php');

class Occupation_types_model extends Base_module_model {
    public $foreign_keys = array('creator_id' => 'fuel_users_model');
    public $required = array(
        'name' => 'Name is required.',
    );

    function __construct()
    {
        parent::__construct('occupation_type');
    }

    function form_fields($values = array())
    {
        $fields = parent::form_fields($values);

        // ******************* ADD CUSTOM FORM STUFF HERE *******************
        $fields['creator_id']['label'] = 'Creator';

        return $fields;
    }
}

class Occupation_type_model extends Base_module_record {

}
