<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(FUEL_PATH.'models/base_module_model.php');

class Positions_model extends Base_module_model {
    public $foreign_keys = array('creator_id' => 'fuel_users_model');
    public $required = array(
        'name' => 'Name is required.',
    );

    function __construct()
    {
        parent::__construct('positions');
    }

    function form_fields($values = array())
    {
        $fields = parent::form_fields($values);

        // ******************* ADD CUSTOM FORM STUFF HERE *******************
        $fields['creator_id']['label'] = 'Creator';
        $fields['image']['folder'] = 'positions';
        $fields['image']['required'];

        return $fields;
    }
}

class Position_model extends Base_module_record {

}
