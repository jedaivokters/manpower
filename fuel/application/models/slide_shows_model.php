<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(FUEL_PATH.'models/base_module_model.php');

class Slide_shows_model extends Base_module_model {

    public $foreign_keys = array('user_id' => array(FUEL_FOLDER => 'fuel_users_model'));
    public $required = array(
        'image' => 'Image is required.',
        'user_id' => 'Author is required.',
    );

    function __construct()
    {
        parent::__construct('slideshow');
    }

    function form_fields($values = array())
    {
        $fields = parent::form_fields($values);

        // ******************* ADD CUSTOM FORM STUFF HERE *******************
        $fields['image']['folder'] = 'banners';
        $fields['image']['required'];
        $fields['image']['comment'] = 'Image must be 790x480 in dimension.';
        $fields['user_id']['label'] = 'Author';
        $fields['user_id']['required'];

        return $fields;
    }


}

class Slide_show_model extends Base_module_record {

}
