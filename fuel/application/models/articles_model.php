<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(FUEL_PATH.'models/base_module_model.php');

class Articles_model extends Base_module_model {

    public $foreign_keys = array('author_id' => 'authors_model');
    public $has_many = array('categories' => array(FUEL_FOLDER => 'fuel_categories_model'));


    function __construct()
    {
        parent::__construct('articles');
    }
    function list_items($limit = NULL, $offset = NULL, $col = 'name', $order = 'asc', $just_count = FALSE)
    {
        $this->db->join('authors', 'authors.id = articles.author_id', 'left');
        $this->db->select('articles.id, title, SUBSTRING(content, 1, 50) AS content, authors.name AS author, date_added, articles.published', FALSE);
        $data = parent::list_items($limit, $offset, $col, $order, $just_count);

        // check just_count is FALSE or else $data may not be a valid array
        if (empty($just_count))
        {
          foreach($data as $key => $val)
          {
            $data[$key]['content'] = htmlentities($val['content'], ENT_QUOTES, 'UTF-8');
          }
        }
        return $data;
    }

    function form_fields($values = array())
    {
        $fields = parent::form_fields($values);

        // ******************* ADD CUSTOM FORM STUFF HERE *******************
        $fields['content']['img_folder'] = 'banners';
        $fields['image']['folder'] = 'banners';

        return $fields;
    }
    function tree()
    {
        return $this->_tree('has_many');
    }


}

class Article_model extends Base_module_record {

}
