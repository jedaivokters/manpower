<?php 
/*
|--------------------------------------------------------------------------
| MY Custom Layouts
|--------------------------------------------------------------------------
|
| specify the name of the layouts and their fields associated with them
*/

$config['default_layout'] = 'main';
$config['layouts_folder'] = '_layouts';

$main_layout = new Fuel_layout('main');
$main_layout->set_description('This is the Main layout field.');
$main_layout->set_label('main');

$news_options = array();

foreach(fuel_model('news_types') as $row) {
    $news_options[$row->id] = $row->name;
}

$options = array('no' => 'No', 'yes' => 'Yes');
$search_type = array(
    'Permanent Recruitment' => 'Permanent Recruitment',
    'Executive Search' => 'Executive Search',
    'Borderless Talent Solutions' => 'Borderless Talent Solutions',
);
$main_fields = array(
    'Header' => array('type' => 'fieldset', 'label' => 'Header', 'class' => 'tab'),
    'page_title' => array('label' => lang('layout_field_page_title')),
    'meta_description' => array('label' => lang('layout_field_meta_description')),
    'meta_keywords' => array('label' => lang('layout_field_meta_keywords')),
	'meta_robot_follow' => array('label' => 'Enable "No" follow link?', 'type' => 'select', 'options' => $options),
    'Body' => array('type' => 'fieldset', 'label' => 'Body', 'class' => 'tab'),
    'body' => array('label' => lang('layout_field_body'), 'type' => 'textarea', 'description' => lang('layout_field_body_description')),
    'body_class' => array('label' => lang('layout_field_body_class')),
    'Sections'  => array('type' => 'fieldset', 'label' => 'Bottom Middle Section', 'class' => 'tab'),
    'bottom_middle_section' => array(
                    'type'          => 'template',
                    'display_label' => FALSE,
                    'label'         => 'Test',
                    'add_extra'     => FALSE,
                    'repeatable'    => TRUE,
                    'title_field'   => 'block',
                    'init_display'   => 'none',
                    'fields'        => array(
                            'section'   => array('type' => 'section', 'value' => 'Content <span class="num">{num}</span>'),
                            'block'     => array('type' => 'block', 'folder' => 'sections'),
                            'title'   => array('label' => 'Title'),
                            'content'   => array('type' => 'wysiwyg', 'editor' => 'ckeditor', 'label' => 'Content'),
                            'image_left'=> array('type' => 'asset', 'label' => 'Image left', 'comment'=> 'Image before the content'),
                        ),
                    ),
    'Sections2'  => array('type' => 'fieldset', 'label' => 'Requisition Section', 'class' => 'tab'),
    'requisition_section' => array(
                    'type'          => 'template',
                    'display_label' => FALSE,
                    'label'         => 'Test',
                    'add_extra'     => FALSE,
                    'repeatable'    => TRUE,
                    'title_field'   => 'block',
                    'init_display'   => 'none',
                    'fields'        => array(
                            'section2'   => array('type' => 'section', 'value' => 'Process <span class="num">{num}</span>'),
                            'image_left2'=> array('type' => 'asset', 'label' => 'Image', 'comment'=> 'Image before the content'),
                            'content2'   => array('type' => 'wysiwyg', 'editor' => 'ckeditor', 'label' => 'Content'),
                        ),
                    ),
    'Job Search'  => array('type' => 'fieldset', 'label' => 'Job Search', 'class' => 'tab'),
    'job_search_main' => array('label'=> 'Enable Search?', 'type' => 'select', 'options' => $options),
    'job_search_type' => array('label'=> 'Search type', 'type' => 'select', 'options' => $search_type),
    'job_search_icon' => array('label'=> 'Enable Search Icons?', 'type' => 'select', 'options' => $options),
    'Contact Form'  => array('type' => 'fieldset', 'label' => 'Contact Form', 'class' => 'tab'),
    'contact_form' => array('label'=> 'Enable Form?', 'type' => 'select', 'options' => $options),
    'employer_body' => array('label' => 'Employer Section Content', 'type' => 'textarea'),
    'employer_side' => array('label' => 'Employer Side Section Content', 'type' => 'textarea'),
    'job_seekerbody' => array('label' => 'Job Seeker Section Content', 'type' => 'textarea'),
    'job_seekerside' => array('label' => 'Job Seeker Side Section Content', 'type' => 'textarea'),
    'News Section'  => array('type' => 'fieldset', 'label' => 'News', 'class' => 'tab'),
    'news' => array('label'=> 'Enable?', 'type' => 'select', 'options' => $options),
    'news_type' => array('label'=> 'Select news type', 'type' => 'select', 'options' => $news_options),
);

$main_layout->add_fields($main_fields);

// Added to the 'layouts' key
$config['layouts']['main'] = $main_layout;


/* End of file MY_fuel_layouts.php */
/* Location: ./application/config/MY_fuel_layouts.php */

