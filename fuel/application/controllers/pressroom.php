<?php
require_once(FUEL_PATH.'/libraries/Fuel_base_controller.php');

class Pressroom extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
        function detail($id = NULL) {
			
	    $this->load->helper('url');
		$this->load->helper('text');

            $this->load->module_model(FUEL_FOLDER, 'news_model');

            $news = $this->news_model->find_one(array(
                'published' => 'yes',
                'id' => $id,
            ));


            $vars = array(
                'title' => $news->title,
                'content' => $news->content,
                'id' => $news->id,
                'image' => $news->image,
                'og_content' => word_limiter($news->content, 100),
                'og_title' => word_limiter($news->title, 100),
            );
            
            $this->fuel->pages->render('pressroom/detail', $vars, array('render_mode' => 'cms', 'layout' =>'pressroom'));
       
        }
}
