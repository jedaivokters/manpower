<?php
require_once(FUEL_PATH . '/libraries/Fuel_base_controller.php');

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();

    }

    /**
     * Profile Home and Preview
     *
     * @todo Resume upload date
     */
    function index() {

        if( ! $this->session->userdata('is_logged_in') ) {
            redirect( site_url('profile/login') );
        }
		
		$q = $this->db->get_where('applicants',array('id'=>$this->session->userdata('user_id')));
		if( $q->num_rows() > 0 ) {
			
			$vars['firstname'] = $q->row()->firstname;
			$vars['middlename'] = $q->row()->middlename;
			$vars['lastname'] = $q->row()->lastname;
			$vars['email_address'] = $q->row()->email_address;
			$vars['street_address'] = $q->row()->street_address;
			$vars['street_address2'] = $q->row()->street_address2;
			$vars['city'] = $q->row()->city;
			$vars['province'] = $q->row()->province;
			$vars['zipcode'] = $q->row()->zipcode;
			$vars['contact_no'] = $q->row()->contact_no;
			$vars['contact_no2'] = $q->row()->contact_no2;
			$vars['attachfile'] = $q->row()->attachfile;
			$vars['status'] = $q->row()->status;
			$vars['last_login'] = $q->row()->last_login;
			$vars['skills'] = $this->get_skills();
			$vars['experiences'] = $this->get_experience();
			$vars['education'] = $this->get_education();
			
			$vars['countries'] = $this->db->get('lt_country')->result();
			$vars['cities'] = $this->db->get('lt_phil_city')->result();
			$vars['provinces'] = $this->db->get('lt_phil_province')->result();
			$vars['degrees'] = $this->db->get('lt_degrees')->result();
			$vars['skills_list'] = $this->db->get('lt_skill')->result();
			$vars['industries'] = $this->db->get('lt_industry')->result();
            $vars['applications'] = $this->db->select('job_applicants.*,job_posts.*')->join('job_posts','job_posts.id = job_applicants.job_post_id')->get_where('job_applicants',array('applicant_id'=>$this->session->userdata('user_id')))->result();

		}
		
        $vars['page_title'] = 'My Profile';
        $this->_out('_layouts/profile',$vars);
    }


    /**
     * Edit of applicant info. With change password function
     *
     * @todo
     */
    function edit() {
        
        if( ! $this->session->userdata('is_logged_in') ) {
            redirect( site_url('profile/login') );
        }

//        $vars['custom'] = new Custom;
		
        $q = $this->db->get_where('applicants',array('id'=>$this->session->userdata('user_id')));
        $u = $q->row();
        $vars['firstname'] = $u->firstname;
        $vars['middlename'] = $u->middlename;
        $vars['lastname'] = $u->lastname;
        $vars['email_address'] = $u->email_address;
        $vars['street_address'] = $u->street_address;
        $vars['street_address2'] = $u->street_address2;
        $vars['city'] = $u->city;
        $vars['province'] = $u->province;
        $vars['zipcode'] = $u->zipcode;
        $vars['contact_no'] = $u->contact_no;
        $vars['contact_no2'] = $u->contact_no2;
        $vars['password'] = $u->password;
        
        $vars['jp_position_type'] = $u->jp_position_type;
        $vars['jp_prefered_location'] = $u->jp_prefered_location;
        $vars['jp_prefered_areas_abroad'] = $u->jp_prefered_areas_abroad;
        $vars['jp_prefered_areas_local'] = $u->jp_prefered_areas_local;
        $vars['jp_relocate_philippines'] = $u->jp_relocate_philippines;
        $vars['jp_relocate_abroad'] = $u->jp_relocate_abroad;
        $vars['jp_industry_id_a'] = $u->jp_industry_id_a;
        $vars['jp_industry_id_b'] = $u->jp_industry_id_b;
        $vars['jp_industry_id_c'] = $u->jp_industry_id_c;
        $vars['jp_expected_salary'] = $u->jp_expected_salary;
        $vars['jp_start_date'] = $u->jp_start_date;

        $position_types = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "applicants" AND COLUMN_NAME = "jp_position_type"')->result();
        $prefered_locations = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "applicants" AND COLUMN_NAME = "jp_prefered_location"')->result();
        $relocate_philippines = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "applicants" AND COLUMN_NAME = "jp_relocate_philippines"')->result();
        $relocate_abroed = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "applicants" AND COLUMN_NAME = "jp_relocate_abroad"')->result();
        $prefered_areas_abroad = array(
            'Any', 'Australia & New Zealand', 'China', 'Hong Kong', 'India', 'Indonesia', 'Japan', 'Malaysia',
            'Singapore', 'Thailand', 'Vietnam', 'Asia - Middle East', 'Asia - Others', 'Africa', 'Europe', 'North America', 'South America'
        );
        $prefered_areas_local = array(
            'Any',
            'Metro Manila / NCR' => array(
                'Caloocan', 'Las Pinas', 'Makati', 'Malabon', 'Mandaluyong', 'Manila', 'Marikina', 'Muntinlupa', 'Navotas',
                'Parañaque', 'Pasay', 'Pasig', 'Pateros', 'Quezon City', 'San Juan', 'Taguig', 'Valenzuela'
            ),
            'CALABARZON' => array(
                'Batangas', 'Cavite', 'Laguna', 'Lucena', 'Quezon', 'Rizal'
            ),
            'Others' => array(
                'ARMM', 'Bicol Region', 'C.A.R.', 'Cagayan Valley', 'Calabarzon @ Mimaropa', 'Caraga', 'Central Luzon', 'Central Visayas',
                'Davao', 'Eastern Visayas', 'Ilocos Region', 'Northern Mindano', 'Soccskargen', 'Western Visayas', 'Zamboanga'
            )
        );
        $vars['list_jp_position_type'] = explode(',', substr($position_types[0]->COLUMN_TYPE, 5, -1));
        $vars['list_jp_prefered_location'] = explode(',', substr($prefered_locations[0]->COLUMN_TYPE, 5, -1));
        $vars['list_jp_prefered_areas_abroad'] = $prefered_areas_abroad;
        $vars['list_jp_prefered_areas_local'] = $prefered_areas_local;
        $vars['list_jp_relocate_philippines'] = explode(',', substr($relocate_philippines[0]->COLUMN_TYPE, 5, -1));
        $vars['list_jp_relocate_abroad'] = explode(',', substr($relocate_abroed[0]->COLUMN_TYPE, 5, -1));
        $vars['list_jp_industry'] = $this->db->get('lt_industry')->result();
        
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'trim');
        $this->form_validation->set_rules('street_address', 'Street Address', 'trim|required');
        $this->form_validation->set_rules('street_address2', 'Street Address 2', 'trim');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('province', 'Province', 'trim');
        $this->form_validation->set_rules('zipcode', 'Zip Code', 'trim|required');
        $this->form_validation->set_rules('contact_no', 'Contact Info', 'trim|required');
        $this->form_validation->set_rules('contact_no2', 'Contact Info 2', 'trim');
        $this->form_validation->set_rules('jp_position_type', 'Position Type', 'trim');
        $this->form_validation->set_rules('jp_prefered_location', 'Prefered Location', 'trim');
        $this->form_validation->set_rules('jp_prefered_areas_abroad', 'Prefered areas abroad', 'trim');
        $this->form_validation->set_rules('jp_prefered_areas_local', 'Prefered local areas', 'trim');
        $this->form_validation->set_rules('jp_relocate_philippines', 'Relocate Philippines', 'trim');
        $this->form_validation->set_rules('jp_relocate_abroad', 'Relocate Abroad', 'trim');
        $this->form_validation->set_rules('jp_industry_id_a', 'Prefered Industry A', 'trim');
        $this->form_validation->set_rules('jp_industry_id_b', 'Prefered Industry B', 'trim');
        $this->form_validation->set_rules('jp_industry_id_c', 'Prefered Industry C', 'trim');
        $this->form_validation->set_rules('jp_expected_salary', 'Expected Salary', 'trim');
        $this->form_validation->set_rules('jp_start_date', 'Date of availability', 'trim');
        
        if ($this->form_validation->run() == TRUE) {
            
            $data = array(
                'firstname' => $this->input->post('firstname'),
                'middlename' => $this->input->post('middlename'),
                'lastname' => $this->input->post('lastname'),
                'street_address' => $this->input->post('street_address'),
                'street_address2' => $this->input->post('street_address2'),
                'city' => $this->input->post('city'),
                'province' => $this->input->post('province'),
                'zipcode' => $this->input->post('zipcode'),
                'contact_no' => $this->input->post('contact_no'),
                'contact_no2' => $this->input->post('contact_no2'),
                'jp_position_type' => $this->input->post('jp_position_type'),
                'jp_prefered_location' => $this->input->post('jp_prefered_location'),
                'jp_prefered_areas_abroad' => $this->input->post('jp_prefered_areas_abroad'),
                'jp_prefered_areas_local' => $this->input->post('jp_prefered_areas_local'),
                'jp_relocate_philippines' => $this->input->post('jp_relocate_philippines'),
                'jp_relocate_abroad' => $this->input->post('jp_relocate_abroad'),
                'jp_industry_id_a' => $this->input->post('jp_industry_id_a'),
                'jp_industry_id_b' => $this->input->post('jp_industry_id_b'),
                'jp_industry_id_c' => $this->input->post('jp_industry_id_c'),
                'jp_expected_salary' => $this->input->post('jp_expected_salary'),
                'jp_start_date' => $this->input->post('jp_start_date')
            );

            // upload resume
            $this->load->library('upload', array(
                'upload_path' => './fuel/resumes/',
                'allowed_types' => 'doc|docx|pdf|png|txt',
                'encrypt_name' =>  TRUE
            ));

            if( $this->upload->do_upload('attachfile') ) {
                $upload_data = $this->upload->data();
                $data['attachfile'] = $upload_data['file_name'];
            }
            
            $this->db->where(array('id'=>$this->session->userdata('user_id')));
            $this->db->update( 'applicants', $data );
            
            redirect( site_url('profile?status=edit_success') );
        } else {
            echo validation_errors(); 
        }
        

        $vars['page_title'] = 'My Profile - Edit';
        $this->_out('_layouts/profile_edit',$vars);
    }

    /**
     * Registration page
     *
     * @todo sending email activation code
     */
    function register() {

        $vars['page_title'] = 'Register';

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_message('is_unique', 'The %s you entered is already registered.');

        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email|is_unique[applicants.email_address]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'trim');
        $this->form_validation->set_rules('street_address', 'Street Address', 'trim|required');
        $this->form_validation->set_rules('street_address2', 'Street Address 2', 'trim');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('province', 'Province', 'trim');
        $this->form_validation->set_rules('zipcode', 'Zip Code', 'trim|required');
        $this->form_validation->set_rules('contact_no', 'Contact Info', 'trim|required');
        $this->form_validation->set_rules('contact_no2', 'Contact Info 2', 'trim');


        if ($this->form_validation->run() == TRUE) {

			$code = rand(100000,999999);
			$hash = sha1($this->input->post('email_address'));
            $data = array(
                'created' => gmdate('Y-m-d H:i:s'),
                'firstname' => $this->input->post('firstname'),
                'middlename' => $this->input->post('middlename'),
                'lastname' => $this->input->post('lastname'),
                'email_address' => $this->input->post('email_address'),
                'password' => sha1($this->input->post('password')),
                'activationcode' => $code,
                'street_address' => $this->input->post('street_address'),
                'street_address2' => $this->input->post('street_address2'),
                'city' => $this->input->post('city'),
                'province' => $this->input->post('province'),
                'zipcode' => $this->input->post('zipcode'),
                'contact_no' => $this->input->post('contact_no'),
                'contact_no2' => $this->input->post('contact_no2'),
				'hash' => $hash
            );

            // upload resume
            $this->load->library('upload', array(
                'upload_path' => './fuel/resumes/',
                'allowed_types' => 'doc|docx|pdf|png|txt',
                'encrypt_name' =>  TRUE
            ));

            if( $this->upload->do_upload('attachfile') ) {
                $upload_data = $this->upload->data();
                $data['attachfile'] = $upload_data['file_name'];
            }


            $this->db->insert('applicants',$data);
            $this->session->set_userdata('email_address',$data['email_address']);
			
			// email activation code
			$this->load->library('email');
			$this->email->from('no-reply@primemanpower.com', 'Prime Manpower');
			$this->email->to($data['email_address']); 
			$this->email->subject('Prime Manpower Member Activation Code');
			$this->email->message('Use the activation code below to activate your account:'."\n\n".$code."\n\n".site_url('profile/activate/'.$hash));

			$this->email->send();
			
            redirect( site_url('profile/activate/'.$hash.'?status=registered') );

        } else {

            $vars['form_errors'] = TRUE;
        }


        $this->_out('_layouts/profile_register',$vars);
    }
    
    function change_password() {
    
        $data = array();
		
		$u = $this->db->get_where('applicants',array('id'=>$this->session->userdata('user_id')))->row();
        
		if( sha1( $this->input->post('password') ) == $u->password ) {
        
            if( $this->input->post('password2') == $this->input->post('password3') and $this->input->post('password2') != '' ) {
            
                $this->db->where(array('id'=>$this->session->userdata('user_id')));
                $this->db->update('applicants',array(
                    'password' => sha1( $this->input->post('password2') )
                ));
                
                $data['status'] = 'success';
                $data['message'] = 'Password changed!';
                
            } else {
            
                $data['status'] = 'failed';
                $data['message'] = $this->input->post('password2').'-'.$this->input->post('password3').'-Invalid new password and password confirmation.';
            }
        } else {
        
            $data['status'] = 'failed';
            $data['message'] = 'Wrong password. Please enter your correct password.';
        }
		
		
		if( $this->input->is_ajax_request() ) {
			echo json_encode($data);
			exit;
		}
    }
    
    function change_email() {
        
        $data= array();
        
        $u = $this->db->get_where('applicants',array('id'=>$this->session->userdata('user_id')))->row();
        
        $q = $this->db->get_where('applicants',array(
            'email_address' => $this->input->post('email_address'),
            'id !=' => $u->id
        ));
        
        if( $q->num_rows() > 0 ) {
            
            $data['status'] = 'failed';
            $data['message'] = 'ERROR: Email already in use by another user';
        } else {
        
            if( $u->email_address != $this->input->post('email_address') ) {
            
                $code = rand(100000,999999);
                $hash = sha1($this->input->post('email_address'));
                $this->db->where(array('id'=>$this->session->userdata('user_id')));
                $this->db->update('applicants',array(
                    'activationcode' => $code,
                    'hash' => $hash,
                    'status' => 'Pending',
                    'email_address' => $this->input->post('email_address')
                ));
                
                // email activation code
                $this->load->library('email');
                $this->email->from('no-reply@primemanpower.com', 'Prime Manpower');
                $this->email->to( $this->input->post('email_address') );
                $this->email->subject('Prime Manpower Member Activation Code');
                $this->email->message('Use the activation code below to activate your account:'."\n\n".$code."\n\n".site_url('profile/activate/'.$hash));
                $this->email->send();
                
                // destroy user ssion logging user out
                $this->session->sess_destroy();
                
                $data['status'] = 'success';
                $data['message'] = 'Email changed. You will be logged off automatically';
                $data['redirect'] = site_url('profile/logout');
            }
        }
        
		if( $this->input->is_ajax_request() ) {
			echo json_encode($data);
			exit;
		}
    }

    function _check_activationcode( $str ) {

        $query = $this->db->get_where(
            'applicants',
            array(
                'hash' => $this->input->post('hash'),
                'activationcode' => $str
            )
        );

        if( $query->num_rows() <= 0 ) {
            $this->form_validation->set_message('_check_activationcode', 'Invalid activation code');

            return FALSE;
        }

        return TRUE;
    }

    /**
     * Activate account
     *
     * @todo Redirect activated users trying to access this page
     */
    function activate($hash =0) {

        $vars['hash'] = $hash;

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('activationcode', 'Activation Code', 'trim|required|callback__check_activationcode');
        $this->form_validation->set_rules('hash', 'Hash', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            // login user
            $q = $this->db->get_where('applicants',array('hash'=>$this->input->post('hash')));
            if( $q->num_rows() > 0 ) {
				
				$this->db->where('id',$q->row()->id);
				$this->db->update('applicants',array(
					'status'=>'Active',
					'last_login' => gmdate('Y-m-d H:i:s')
				));
                // set session data
                $this->session->set_userdata(array(
                    'is_logged_in' => TRUE,
                    'user_id' => $q->row()->id,
                ));

                redirect( site_url('profile?status=new_activation') );
            }

            redirect( site_url('/') );

        } else {
            $vars['form_errors'] = TRUE;
        }

        $vars['page_title'] = 'Activate';
        $this->_out('_layouts/profile_activate',$vars);
    }
	
	function add_skill() {
		
		$data = array();
		
		$q = $this->db->get_where('applicants_skills',array('applicants_id'=>$this->session->userdata('user_id')));
		if( $q->num_rows() < 3 ) {
			
			$this->db->insert('applicants_skills', array(
				'applicants_id' => $this->session->userdata('user_id'),
				'skill_id' => $this->input->post('skill_id'),
				'title' => $this->input->post('title'),
				'proficiency' => $this->input->post('proficiency'),
				'yrs_exp' => $this->input->post('yrs_exp')
			));

			if( $this->db->insert_id() ) {
				$data['status'] = 'success';
			}
		} else {
		
			$data['status'] = 'failed';
			$data['message'] = 'You can have upto Three(3) skills listed only.';
		}
		
		
		if( $this->input->is_ajax_request() ) {
			echo json_encode($data);
			exit;
		}
	}
	
	/**
	 * @todo logic for non-ajax return
	 */
	function delete_skill( $id ) {
		$this->db->delete('applicants_skills',array('id'=>$id));
		if( $this->input->is_ajax_request() ) {
			echo json_encode(array(
				'status' => 'success'
			));
			exit;
		}
	}
	
	function get_skills() {
		$str = '';
		$this->db->select('applicants_skills.*, lt_skill.skill_name as skill_name');
		$this->db->join('lt_skill','lt_skill.skill_id = applicants_skills.skill_id','left');

		$q = $this->db->get_where('applicants_skills',array('applicants_id'=>$this->session->userdata('user_id')));
		if( $q->num_rows() > 0 ) {
			$vars['skills'] = $q->result();
			$str = $this->load->view('_blocks/profile_index_skills',$vars,true);
		}
		if( $this->input->is_ajax_request() ) {
			echo json_encode(array(
				'status' => 'success',
				'html' => $str
			));
			exit;
		}
		
		return $str;
	}
	
	function add_education() {
		$data = array();
		
		$record = array(
			'applicants_id' => $this->session->userdata('user_id'),
			'degree_id' => $this->input->post('degree_id'),
			'degree_others' => $this->input->post('degree_others'),
			'school' => $this->input->post('school'),
			'country_id' => $this->input->post('country_id'),
			'city_id' => $this->input->post('city_id'),
			'province_id' => $this->input->post('province_id'),
			'city_name' => $this->input->post('city_name'),
			'province_name' => $this->input->post('province_name'),
			'start_date' => date('Y-m-d',strtotime($this->input->post('start_date'))),
			'end_date' => date('Y-m-d',strtotime($this->input->post('end_date')))
		);
		
		
		if( $this->input->post('education_id') == '' ) {
			
			$this->db->insert('applicants_education', $record);

			if( $this->db->insert_id() ) {
				$data['status'] = 'success';
			}
		} else {
			
			$this->db->where(array('id'=>$this->input->post('education_id')));
			$this->db->update('applicants_education', $record);
			
			$data['status'] = 'success';
		}
		
		
		if( $this->input->is_ajax_request() ) {
			echo json_encode($data);
			exit;
		}
	}
	
	/**
	 * @todo logic for non-ajax return
	 */
	function delete_education( $id ) {
		$this->db->delete('applicants_education',array('id'=>$id));
		
		if( $this->input->is_ajax_request() ) {
			echo json_encode(array(
				'status' => 'success'
			));
			exit;
		}
	}
	
	function get_education() {
		$str = '';
		$this->db->select('applicants_education.*,lt_country.country_name,lt_degrees.degree_title');
		$this->db->join('lt_country','lt_country.country_id = applicants_education.country_id');
		$this->db->join('lt_degrees','lt_degrees.degree_id = applicants_education.degree_id','left');
		$q = $this->db->get_where('applicants_education',array('applicants_id'=>$this->session->userdata('user_id')));
		if( $q->num_rows() > 0 ) {
			$vars['educations'] = $q->result();
			$str = $this->load->view('_blocks/profile_index_education',$vars,true);
		}
		if( $this->input->is_ajax_request() ) {
			echo json_encode(array(
				'status' => 'success',
				'html' => $str
			));
			exit;
		}
		
		return $str;
	}
	
	function add_experience() {
		$data = array();
		
		$record = array(
			'applicants_id' => $this->session->userdata('user_id'),
			'job_title' => $this->input->post('job_title'),
			'company' => $this->input->post('company'),
			'location' => $this->input->post('location'),
			'industry' => $this->input->post('industry'),
			'contact_no' => $this->input->post('contact_no'),
			'job_description' => $this->input->post('job_description'),
			'start_date' => date('Y-m-d',strtotime($this->input->post('start_date'))),
			'end_date' => date('Y-m-d',strtotime($this->input->post('end_date')))
		);
		
		if( $this->input->post('experience_id') == '' ) {
			$this->db->insert('applicants_work_experiences', $record);

			if( $this->db->insert_id() ) {
				$data['status'] = 'success';
			}
		} else {
			
			$this->db->where(array('id'=>$this->input->post('experience_id')));
			$this->db->update('applicants_work_experiences', $record);
			
			$data['status'] = 'success';
		}
		
		
		if( $this->input->is_ajax_request() ) {
			echo json_encode($data);
			exit;
		}
	}
	
	/**
	 * @todo logic for non-ajax return
	 */
	function delete_experience( $id ){
		$this->db->delete('applicants_work_experiences',array('id'=>$id));
		
		if( $this->input->is_ajax_request() ) {
			echo json_encode(array(
				'status' => 'success'
			));
			exit;
		}
	}
	
	function get_experience() {
		$str = '';
		$q = $this->db->get_where('applicants_work_experiences',array('applicants_id'=>$this->session->userdata('user_id')));
		if( $q->num_rows() > 0 ) {
			$vars['experiences'] = $q->result();
			$str = $this->load->view('_blocks/profile_index_experience',$vars,true);
		}
		if( $this->input->is_ajax_request() ) {
			echo json_encode(array(
				'status' => 'success',
				'html' => $str
			));
			exit;
		}
		
		return $str;
	}

    function _check_login( $str ) {

        $q = $this->db->get_where(
            'applicants',
            array(
                'email_address' => $this->input->post('email_address'),
                'password' => sha1($str),
                'status' => 'Active'
            )
        );

        if( $q->num_rows() <= 0 ) {
            $this->form_validation->set_message('_check_login', 'The Email Address or Password you entered is invalid');

            return FALSE;
        }

        return TRUE;
    }

    function login() {
        $vars['page_title'] = 'Register';

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback__check_login');

        if ($this->form_validation->run() == TRUE) {
            // login user
            $q = $this->db->get_where('applicants',array('email_address'=>$this->input->post('email_address')));

            if( $q->num_rows() > 0 ) {

                // set session data
                $this->session->set_userdata(array(
                    'is_logged_in' => TRUE,
                    'user_id' => $q->row()->id,
                    'email_address' => $this->input->post('email_address')
                ));
				
				// update last login
				$this->db->where(array('id'=>$q->row()->id));
				$this->db->update('applicants',array('last_login'=>gmdate('Y-m-d H:i:s')));

                
                if( $this->input->get('redirect') ) {
                    redirect( $this->input->get('redirect') );
                } else {
                    redirect( site_url('profile?status=logged_in') );
                }
            }

            redirect( site_url('/') );

        } else {
            $vars['form_errors'] = TRUE;
        }


        $vars['page_title'] = 'Login';
        $this->_out('_layouts/profile_login',$vars);
    }

    function logout() {
		
		// update last logout
		$this->db->where(array('id'=>$this->session->userdata('user_id')));
		$this->db->update('applicants',array('last_logout'=>gmdate('Y-m-d H:i:s')));

        $this->session->sess_destroy();

        redirect( site_url('?status=user_logged_out') );
    }

    /**
     * Retireve password
     *
     * @params String $email email_address sha1 hash
     * @todo see notes below
     */
    function reset_password( $hash = '' ) {
        
        // @todo
        //
        // * flag logic to check is hash key is existing
        // * this should prompt the system to create a new password
        // * send the new password to email
        if( $hash ) {
        
            $q = $this->db->get_where('applicants',array('hash'=>$hash));
            if( $q->num_rows() > 0 ) {
                
                // send new password thru email here
            } else {
            
                // hack attempt
            }
            
            redirect( site_url('profile/login') );
        }

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');

        if ($this->form_validation->run() == TRUE) {
            // login user
            $q = $this->db->get_where('applicants',array('email_address'=>$this->input->post('email_address')));

            if( $q->num_rows() > 0 ) {

                // @todo
                //
                // * send password reset link thru email with email address hash as reference
                
            }

            redirect( site_url('/') );

        } else {
            $vars['form_errors'] = TRUE;
        }
        

        $vars['page_title'] = 'Reset Password';
        $this->_out('_layouts/profile_resetpassword',$vars);
    }
	
	function apply( $job_id ) {
		
		$data = array();
        
        if( !$this->session->userdata('is_logged_in') ) {

            $data['form'] = '';
            $data['message'] = 'Error: You are not logged in. Please <a href="'.site_url('profile/login?redirect='.$_SERVER['HTTP_REFERER']).'">login</a> to continue.';
            
        } else {
        
            if( $job_id ) {
                $q = $this->db->get_where('job_applicants',array(
                    'job_post_id' => $job_id,
                    'applicant_id' => $this->session->userdata('user_id')
                ));

                if( $q->num_rows() > 0 ) {

                    $data['status'] = 'failed';
                    $data['message'] = 'Error: You have already applied for this job.';
                } else {

//                    if (! isset($_POST['bypass'])) {
//                        $position_type = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "job_applicants" AND COLUMN_NAME = "jp_position_type"')->result();
//                        $location = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "job_applicants" AND COLUMN_NAME = "jp_prefered_location"')->result();
//                        $yesno = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "job_applicants" AND COLUMN_NAME = "jp_relocate_philippines"')->result();
//                        $vars['position_types'] = explode(',', substr($position_type[0]->COLUMN_TYPE, 5, -1));
//                        $vars['locations'] = explode(',', substr($location[0]->COLUMN_TYPE, 5, -1));
//                        $vars['yesno'] = explode(',', substr($yesno[0]->COLUMN_TYPE, 5, -1));
//                        $vars['industries'] = $this->db->get('lt_industry')->result();
//                        $vars['j_id'] = $job_id;
//
//                        //Show Job Preference Form
//                        $data['message'] = $this->load->view('_blocks/sections/form_profile_job_preferences', $vars, TRUE);
//                        $data['status'] = 'success';
//                        echo json_encode($data);
//                        return ;
//                    }
                    
                    $user = $this->db->get_where('applicants',array('id'=>$this->session->userdata('user_id')))->row();

                    $this->db->insert('job_applicants', array(
                        'applicant_id' => $this->session->userdata('user_id'),
                        'firstname' => $user->firstname,
                        'middlename' => $user->middlename,
                        'lastname' => $user->lastname,
                        'email_address' => $user->email_address,
                        'street_address' => $user->street_address,
                        'street_address2' => $user->street_address2,
                        'city' => $user->city,
                        'province' => $user->province,
                        'zipcode' => $user->zipcode,
                        'contact_no' => $user->contact_no,
                        'contact_no2' => $user->contact_no2,
                        'attachfile' => $user->attachfile,
                        'jp_position_type' => $user->jp_position_type,
                        'jp_prefered_location' => $user->jp_prefered_location,
                        'jp_prefered_areas_abroad' => $user->jp_prefered_areas_abroad,
                        'jp_prefered_areas_local' => $user->jp_prefered_areas_local,
                        'jp_relocate_philippines' => $user->jp_relocate_philippines,
                        'jp_relocate_abroad' => $user->jp_relocate_abroad,
                        'jp_industry_id_a' => $user->jp_industry_id_a,
                        'jp_industry_id_b' => $user->jp_industry_id_b,
                        'jp_industry_id_c' => $user->jp_industry_id_c,
                        'jp_expected_salary' => $user->jp_expected_salary,
                        'jp_start_date' => $user->jp_start_date,
                        'job_post_id' => $job_id,
                        'date_added' => gmdate('Y-m-d H:i:s')
                    ));

                    if( $this->db->insert_id() ) {
                        $data['status'] = 'success';
                        $data['message'] = 'Application successful!';
                    }
                }

            } else {
                $data['status'] = 'failed';
                $data['message'] = 'There was an error processing your request. Please refresh this page and try again after a few minutes.';
            }

        }
		
		
		if( $this->input->is_ajax_request() ) {
			echo json_encode($data);
			exit;
		}
	}

    private function _out( $template, $vars = array() ){

        $page = $this->fuel->pages->create();

        $vars = array_merge( $this->fuel->pagevars->retrieve(), $vars );
        $out = $this->load->view($template,$vars,true);

        $out = $page->fuelify($out);

        echo $out;
    }
}
