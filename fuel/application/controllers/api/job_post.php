<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Job_post extends REST_Controller
{
    function __construct()
    {
        // Construct our parent class
        parent::__construct();
    }
    
    function jobs_post()
    {
        $data = array();
        $sort = $_POST['sort'];
        $order = $_POST['order'];
        $where = array();
        
        $this->db->_protect_identifiers = false;
        
        if (isset($_POST['select'])) {
            $this->db->select($_POST['select'] , FALSE);
        }

        if (isset($_POST['where'])) {
            $where = $_POST['where'];
        }

        if (isset($_POST['join'])) {
            foreach ($_POST['join'] as $j) {
                $this->db->join($j['table'], $j['where'], $j['left']);
            }
        }
        
        $this->db->order_by($order, $sort);

        if (isset($_POST['rows'])) {
            $this->db->limit($_POST['rows'], $_POST['page']);
        }
        
        $jobs = $this->db->get_where('job_posts', $where);

        if ( $jobs->num_rows() > 0) {
            $data = $jobs->result();
        }

        $this->response(array('data' => $data, 'rows' => $jobs->num_rows()), 200);
    }
    
    function job_applicants_post()
    {
        $data = array();
        $sort = $_POST['sort'];
        $order = $_POST['order'];
        $where = array();
        
        $this->db->_protect_identifiers = false;
        
        if (isset($_POST['select'])) {
            $this->db->select($_POST['select'] , FALSE);
        }

        if (isset($_POST['where'])) {
            $where = $_POST['where'];
        }

        if (isset($_POST['join'])) {
            foreach ($_POST['join'] as $j) {
                $this->db->join($j['table'], $j['where'], $j['left']);
            }
        }
        
        $this->db->order_by($order, $sort);

        if (isset($_POST['rows'])) {
            $this->db->limit($_POST['rows'], $_POST['page']);
        }
        
        $jobs = $this->db->get_where('job_applicants', $where);

        if ( $jobs->num_rows() > 0) {
            $data = $jobs->result();
        }

        $this->response(array('data' => $data, 'rows' => $jobs->num_rows()), 200);
    }
    
    function assign_job_order_post()
    {
        $_data = array(
            'job_order_id' => $_POST['new_job_order_id'],
        );

        //Delete old
        $this->db->update('job_posts', array('job_order_id' => NULL), array('job_order_id' => $_POST['new_job_order_id']));
        //Updating to new
        $this->db->update('job_posts', $_data, array('id' => $_POST['id']));

        $this->response(TRUE, 200); // 200 being the HTTP response code
    }

    function download_resume_get($filename) {
        $data = file_get_contents(WEB_ROOT.'fuel/resumes/'.$filename); // Read the file's contents
        echo $data;
    }
}