<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Candidate extends REST_Controller
{
    function __construct()
    {
        // Construct our parent class
        parent::__construct();
    }
    function members_post()
    {
        $data = array();
        $sort = $_POST['sort'];
        $order = $_POST['order'];
        $where = array();
        
        $this->db->_protect_identifiers = false;
        
        if (isset($_POST['select'])) {
            $this->db->select($_POST['select'] , FALSE);
        }

        if (isset($_POST['where'])) {
            $where = $_POST['where'];
        }

        if (isset($_POST['join'])) {
            foreach ($_POST['join'] as $j) {
                $this->db->join($j['table'], $j['where'], $j['left']);
            }
        }
        
        $this->db->order_by($order, $sort);

        if (isset($_POST['rows'])) {
            $this->db->limit($_POST['rows'], $_POST['page']);
        }
        
        $jobs = $this->db->get_where('applicants', $where);

        if ( $jobs->num_rows() > 0) {
            $data = $jobs->result();
            //Add extra data
            foreach($data as $key => $candidate) {
                $indus_data[] = $candidate->jp_industry_id_a;
                $indus_data[] = $candidate->jp_industry_id_b;
                $indus_data[] = $candidate->jp_industry_id_c;
                
                $data[$key]->candidate_skills = implode(', ', $this->_get_skills(3, $candidate->id));
                $data[$key]->candidate_industries = implode(', ', $this->_get_industries($indus_data));

                $this->db->select('id as candidate_job_applicant_id');
                $jobs_applied = $this->db->get_where('job_applicants', array('applicant_id' => $candidate->id ));

                if ($jobs_applied->num_rows() > 0) {
                    $jobs_applied = $jobs_applied->result_array();
                } else {
                    $jobs_applied = array();
                }

                $data[$key]->jobs_applied =  $jobs_applied;
            }
        }
        
        $this->response(array('data' => $data, 'rows' => $jobs->num_rows()), 200);
    }
    
    function _get_skills($limit = 0, $applicants_id = 0) {
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, 0);
        $rec = $this->db->get_where('applicants_skills', array('applicants_id' => $applicants_id));
        $data = array();
        if ($rec->num_rows() > 0 ) {
            foreach ($rec->result() as $r) {
                $data[] = ($r->skill_id == 0 ) ? NULL : $this->db->get_where('lt_skill', array('skill_id' => $r->skill_id ))->row()->skill_name;
            }
        }
        
        return array_filter($data, function ($val) {
            return($val != '');
        });
    }
    
    function _get_industries($industries = array()) {
        $data = array();
        foreach ($industries as $i) {
            $data[] = ($i == 0 ) ? NULL : $this->db->get_where('lt_industry', array('industry_id' => $i ))->row()->industry_name;
        }
        
        return array_filter($data, function ($val) {
            return($val != '');
        });
    }
    
    function applicants_post()
    {
        $data = array();
        $sort = $_POST['sort'];
        $order = $_POST['order'];
        $where = array();
        
        $this->db->_protect_identifiers = false;
        
        if (isset($_POST['select'])) {
            $this->db->select($_POST['select'] , FALSE);
        }

        if (isset($_POST['where'])) {
            $where = $_POST['where'];
        }

        if (isset($_POST['join'])) {
            foreach ($_POST['join'] as $j) {
                $this->db->join($j['table'], $j['where'], $j['left']);
            }
        }
        
        $this->db->order_by($order, $sort);

        if (isset($_POST['rows'])) {
            $this->db->limit($_POST['rows'], $_POST['page']);
        }
        
        $jobs = $this->db->get_where('job_applicants', $where);

        if ( $jobs->num_rows() > 0) {
            $data = $jobs->result();
        }
        
        $this->response(array('data' => $data, 'rows' => $jobs->num_rows()), 200);
    }   
    
    function download_data_post()
    {        
        $data = array();
        $applicants = $this->db->get_where('job_applicants', array('is_synced' => 0, 'id' => $_POST['id']));

        if ($applicants->num_rows() > 0) {
            foreach ($applicants->result_array() as $a) {
                $raw_data['applicant'] = $a;
                $applicants_edu = $this->db->get_where('applicants_education', array('applicants_id' => $a['applicants_id']));
                $applicants_sk = $this->db->get_where('applicants_skills', array('applicants_id' => $a['applicants_id']));
                $applicants_we = $this->db->get_where('applicants_work_experiences', array('applicants_id' => $a['applicants_id']));

                $raw_data['applicant']['educations'] = array();
                $raw_data['applicant']['skills'] = array();
                $raw_data['applicant']['works'] = array();

                if ($applicants_edu->num_rows() > 0) {
                    $raw_data['applicant']['educations'] = $applicants_edu->result_array();
                }
                if ($applicants_sk->num_rows() > 0) {
                    $raw_data['applicant']['skills'] = $applicants_sk->result_array();
                }
                if ($applicants_we->num_rows() > 0) {
                    $raw_data['applicant']['works'] = $applicants_we->result_array();
                }

                $data[] = $raw_data;
            }
        }

        $this->response(array('data' => $data), 200);
    }
    
    function assign_job_order_post()
    {
        $_data = array(
            'job_order_id' => $_POST['new_job_order_id'],
        );

        //Delete old
        $this->db->update('job_posts', array('job_order_id' => NULL), array('job_order_id' => $_POST['new_job_order_id']));
        //Updating to new
        $this->db->update('job_posts', $_data, array('id' => $_POST['id']));

        $this->response(TRUE, 200); // 200 being the HTTP response code
    }

    function download_resume_get($filename) {
        $data = file_get_contents(WEB_ROOT.'fuel/resumes/'.$filename); // Read the file's contents
        echo $data;
    }
}