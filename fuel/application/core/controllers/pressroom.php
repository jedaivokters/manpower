<?php
require_once(FUEL_PATH.'/libraries/Fuel_base_controller.php');

class Pressroom extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
        function detail($id = NULL) {

            $this->load->module_model(FUEL_FOLDER, 'news_model');

            $news = $this->news_model->find_one(array(
                'published' => 'yes',
                'id' => $id,
            ));

//            if (count($news) == 0) {
//                show_404();
//                return;
//            }

            $vars = array(
                'title' => $news->title,
                'content' => $news->content,
                'id' => $news->id,
            );
            
            $this->fuel->pages->render('pressroom/detail', $vars, array('render_mode' => 'cms', 'layout' =>'pressroom'));
            //$this->load->view('pressroom_v',$vars);
            //$this->fuel->pages->render('main/details', $vars,  array('render_mode' => 'cms'));
        }
}
