<?php

require_once(FUEL_PATH . '/libraries/Fuel_base_controller.php');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function search_top() {
        $vars = array(
            'q' => $this->input->post('search_top', TRUE),
            'search_top' => TRUE,
            'job_search_type' => 'all'
        );
        //... form code goes here
        // use Fuel_page to render so it will grab all opt-in variables and do any necessary parsing
        $this->fuel->pages->render('main/search_top', $vars, array('render_mode' => 'cms'));
    }

    function details($id = NULL) {

        $this->load->module_model(FUEL_FOLDER, 'job_posts_model');

        $job_post = $this->job_posts_model->find_one(array(
                    'published' => 'yes',
                    'id' => $id,
                ));

        if (count($job_post) == 0) {
            show_404();
            return;
        }

        $vars = array(
            'j_id' => $job_post->id,
            'j_job_title' => $job_post->title,
            'j_content' => $job_post->content,
            'view_job_details' => TRUE,
            'page_title' => 'Job: ' . $job_post->title
        );

        $this->fuel->pages->render('main/details', $vars, array('render_mode' => 'cms'));
    }

    function apply() {
        $this->load->library('form_validation');
        $this->load->module_model(FUEL_FOLDER, 'job_posts_model');
        $this->load->module_model(FUEL_FOLDER, 'job_applicants_model');

        $id = $this->input->post('id', TRUE);

        $job_post = $this->job_posts_model->find_one(array(
                    'published' => 'yes',
                    'id' => $id,
                ));

        $response = array(
            'success' => FALSE,
            'errors' => array()
        );

        if (count($job_post) == 0) {
            echo json_encode($response);
            exit;
        }
        //Set rules
        $this->form_validation->set_rules('fname', 'First name', 'required');
        $this->form_validation->set_rules('lname', 'Last name', 'required');
        $this->form_validation->set_rules('resume', '', 'resume');
        $this->form_validation->set_rules('saddress', 'Street Address', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'valid_email|required|callback__email_exists[' . $id . ']');
        $this->form_validation->set_rules('city', 'City', 'required');
        //$this->form_validation->set_rules('province', 'Province', 'required');
        $this->form_validation->set_rules('zipcode', 'Zip Code', 'required');
        $this->form_validation->set_rules('contact', 'Contact No', 'required');

        //$this->form_validation->set_rules('resume', 'Attachment', 'callback__attachment_check');
        //Validate form first before uploading
        $valid = $this->form_validation->run();
        $up_data = $this->_upload_attachment($valid);

        //With error it will display the error message
        //Without error it will display empty
        $response['errors']['fname'] = form_error('fname');
        $response['errors']['lname'] = form_error('lname');
        $response['errors']['saddress'] = form_error('saddress');
        $response['errors']['email'] = form_error('email');
        $response['errors']['city'] = form_error('city');
        //$response['errors']['province'] = form_error('province');
        $response['errors']['zipcode'] = form_error('zipcode');
        $response['errors']['contact'] = form_error('contact');
        $response['errors']['resume'] = $up_data['errors'];

        if ($this->form_validation->run() == FALSE || $up_data['errors'] != '') {
            echo json_encode($response);
            exit;
        }


        $applicant['firstname'] = $this->input->post('fname', TRUE);
        $applicant['middlename'] = $this->input->post('mname', TRUE);
        $applicant['lastname'] = $this->input->post('lname', TRUE);
        $applicant['email_address'] = $this->input->post('email', TRUE);
        $applicant['street_address'] = $this->input->post('saddress', TRUE);
        $applicant['street_address2'] = $this->input->post('saddress2', TRUE);
        $applicant['city'] = $this->input->post('city', TRUE);
        $applicant['province'] = $this->input->post('province', TRUE);
        $applicant['zipcode'] = $this->input->post('zipcode', TRUE);
        $applicant['contact_no'] = $this->input->post('contact', TRUE);
        $applicant['contact_no2'] = $this->input->post('contact2', TRUE);
        $applicant['attachfile'] = $up_data['response']['file_name'];
        $applicant['job_post_id'] = $id;
        $applicant['date_added'] = date("Y-m-d H:i:s");

        $this->job_applicants_model->insert($applicant);

        //echo '<pre>';
        //print_r($obj);

        $response['success'] = TRUE;

        echo json_encode($response);
    }

    function contact() {
        $this->load->library('form_validation');

        $response = array(
            'success' => FALSE,
            'sent' => TRUE,
            'errors' => array()
        );

        if (isset($_POST['ce'])) {

            //Set rules
            $this->form_validation->set_rules('ce_fname', 'First name', 'required');
            $this->form_validation->set_rules('ce_lname', 'Last name', 'required');
            $this->form_validation->set_rules('ce_title', 'Title', 'required');
            $this->form_validation->set_rules('ce_phone', 'Phone', 'required');
            $this->form_validation->set_rules('ce_email', 'E-mail', 'valid_email|required');
            $this->form_validation->set_rules('ce_company', 'Company', 'required');

            $this->form_validation->run();

            //With error it will display the error message
            //Without error it will display empty
            $response['errors']['ce_fname'] = form_error('ce_fname');
            $response['errors']['ce_lname'] = form_error('ce_lname');
            $response['errors']['ce_title'] = form_error('ce_title');
            $response['errors']['ce_phone'] = form_error('ce_phone');
            $response['errors']['ce_email'] = form_error('ce_email');
            $response['errors']['ce_company'] = form_error('ce_company');

            if ($this->form_validation->run() == FALSE) {
                echo json_encode($response);
                exit;
            }

            $applicant['c_fname'] = $this->input->post('ce_fname', TRUE);
            $applicant['c_lname'] = $this->input->post('ce_lname', TRUE);
            $applicant['c_title'] = $this->input->post('ce_title', TRUE);
            $applicant['c_phone'] = $this->input->post('ce_phone', TRUE);
            $applicant['c_email'] = $this->input->post('ce_email', TRUE);
            $applicant['c_company'] = $this->input->post('ce_company', TRUE);
            $applicant['c_message'] = $this->input->post('ce_message', TRUE);
            $applicant['c_subject'] = 'Employer Inquiry';
            
        } else {

            //Set rules
            $this->form_validation->set_rules('c_fname', 'First name', 'required');
            $this->form_validation->set_rules('c_lname', 'Last name', 'required');
            $this->form_validation->set_rules('c_title', 'Title', 'required');
            $this->form_validation->set_rules('c_phone', 'Phone', 'required');
            $this->form_validation->set_rules('c_email', 'E-mail', 'valid_email|required');
            $this->form_validation->set_rules('c_company', 'Company', 'required');

            $valid = $this->form_validation->run();
            $up_data = $this->_upload_attachment($valid);

            //With error it will display the error message
            //Without error it will display empty
            $response['errors']['c_fname'] = form_error('c_fname');
            $response['errors']['c_lname'] = form_error('c_lname');
            $response['errors']['c_title'] = form_error('c_title');
            $response['errors']['c_phone'] = form_error('c_phone');
            $response['errors']['c_email'] = form_error('c_email');
            $response['errors']['c_company'] = form_error('c_company');
            //Must be "resume" cause the upload function gets the post data resume please see upload function
            $response['errors']['resume'] = $up_data['errors'];

            
            if ($this->form_validation->run() == FALSE || $up_data['errors'] != '') {
                echo json_encode($response);
                exit;
            }

            $applicant['c_fname'] = $this->input->post('c_fname', TRUE);
            $applicant['c_lname'] = $this->input->post('c_lname', TRUE);
            $applicant['c_title'] = $this->input->post('c_title', TRUE);
            $applicant['c_phone'] = $this->input->post('c_phone', TRUE);
            $applicant['c_email'] = $this->input->post('c_email', TRUE);
            $applicant['c_company'] = $this->input->post('c_company', TRUE);
            $applicant['c_message'] = $this->input->post('c_message', TRUE);
            $applicant['c_subject'] = 'JobSeeker Inquiry';

            //sets attachment here
            $attachment = TRUE;
        }

        $this->load->library('email');
        $config['mailtype'] = 'html';
        //$config['protocol'] = 'smtp';
        $this->email->initialize($config);
        $fullname = $applicant['c_title'] . ' ' . $applicant['c_fname'] . ' ' . $applicant['c_lname'];
        $this->email->from($applicant['c_email'], $fullname);
        //Change to company email
        $this->email->to('allan.bernabe@gmail.com');

        //Attachment
        if (isset($attachment)) {
            $attach_path = WEB_ROOT . '/fuel/resumes/'.$up_data['response']['file_name'];
            $this->email->attach($attach_path);
        }

        $body = "<table border ='1'>
                <tr>
                    <td>Title</td>
                    <td>$applicant[c_title]</td>
                </tr>
                <tr>
                     <td>First Name</td>
                     <td>$applicant[c_fname]</td>
                </tr>
                <tr>
                     <td>Last Name</td>
                     <td>$applicant[c_lname]</td>
                </tr>
                <tr>
                     <td>Contact</td>
                     <td>$applicant[c_phone]</td>
                </tr>
                <tr>
                     <td>Company Email</td>
                     <td>$applicant[c_email]</td>
                </tr>
                <tr>
                     <td>Company</td>
                     <td>$applicant[c_company]</td>
                </tr>
                <tr>
                     <td>Message</td>
                     <td>$applicant[c_message]</td>
                </tr>
            </table>";
        $this->email->subject($applicant['c_subject']);
        $this->email->message($body);

        if (!$this->email->send()) {

            //Remove temporary attachment file from the server
            if (isset($attachment)) {
                if (file_exists($attach_path)) {
                    unlink($attach_path);
                }
            }

            echo $this->email->print_debugger();
            $response['sent'] = FALSE;
            $response['success'] = TRUE;
            echo json_encode($response);
            exit;
        }

        //Remove temporary attachment file from the server
        if (isset($attachment)) {
            if (file_exists($attach_path)) {
                unlink($attach_path);
            }
        }

        $response['success'] = TRUE;
        echo json_encode($response);
    }

    function _attachment_check($str) {
        if (!isset($_FILES['resume'])) {
            $this->form_validation->set_message('_attachment_check', 'The %s field is required');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function _email_exists($str, $id) {
        if ($this->job_applicants_model->record_exists(array(
                    'email_address' => $str,
                    'job_post_id' => $id
                ))) {
            $this->form_validation->set_message('_email_exists', 'The %s field is already exists in this job post.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function _upload_attachment($validation) {
        if (!$validation) {
            //Just blank
            $data['errors'] = '';
            return $data;
        }

        $config['upload_path'] = WEB_ROOT . '/fuel/resumes/';
        $config['allowed_types'] = 'doc|pdf|txt|docx';
        $config['max_size'] = '11000';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        //If has some error
        if (!$this->upload->do_upload('resume')) {
            $data['errors'] = $this->upload->display_errors();
            return $data;
        } else {
            $data['errors'] = '';
            $data['response'] = $this->upload->data();
            return $data;
        }
    }
}