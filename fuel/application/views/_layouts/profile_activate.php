<?php $this->load->view('_blocks/header') ?>
<div class="main_inner">

    <?php if( $this->input->get('status') == 'registered' ):?>
    <h2>Success!</h2>
    <p>Thank you for registering.</p>
    <p>Please check your email to activate your account.</p><br />
    <?php endif;?>
    <p>Enter the code from your email address to activate your account.</p><br />
    <form method="post" enctype="multipart/form-data" action="<?php echo site_url('profile/activate');?>">
        <input type="hidden" name="hash" id="hash" value="<?php echo $hash?>" />
        <input type="text" name="activationcode" id="activationcode" placeholder="Enter Activation Code" />
        <input type="submit" value="Submit" />
        <?php echo form_error('activationcode'); ?>
    </form>

<style type="text/css">
    .field-set {
        padding:0;
        margin:0;
        list-style-type:none;
    }
    .field-set, .action-set {
        margin-top:20px;
    }

    .field-set .form-field {
        margin-top:10px;
    }
    .field-set .form-field:first-child {
        margin-top:0;
    }
    .field-set .form-field:after {
        content: " ";
        display:block;
        clear:both;
    }
    .field-set label {
        float:left;
        min-width:160px;
        font-size:1.1em;
    }
    .field-set .input {
        float:left;
    }
    .field-set .error {
        float:left;
        margin-left:5px;
    }
    .field-set .form-field .long {
        width:320px;
    }
    .field-set .form-field.required label:after {
        content: " *";
        color:#c00;
    }
</style>
<?php $this->load->view('_blocks/footer') ?>
