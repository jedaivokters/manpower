<?php $this->load->view('_blocks/header', array(
	'news_and_article'	=> true,
	'og_content' => $og_content,
	'og_title' => $og_title,
	'og_url' => site_url('/pressroom/detail/'. $id),
)); ?>

<div class="main_inner">
    <h1><b><?php echo $title; ?></b></h1>

    <?php echo $content; ?>

    <br />
    <style type="text/css">

    #share-buttons img {
    width: 35px;
    padding: 5px;
    border: 0;
    box-shadow: 0;
    display: inline;
    }

    </style>
    <!-- I got these buttons from simplesharebuttons.com -->
    <div id="share-buttons">

    <!-- Facebook -->
    <a href="http://www.facebook.com/sharer.php?u=<?php echo site_url('/pressroom/detail/'. $id); ?>" target="_blank"><img src="http://www.simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" /></a>

    <!-- Twitter -->
    <a href="http://twitter.com/share?url=<?php echo site_url('/pressroom/detail/'. $id); ?>&text=<?php echo $title; ?>" target="_blank"><img src="http://www.simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" /></a>

    <!-- Google+ -->
    <a href="https://plus.google.com/share?url=<?php echo site_url('/pressroom/detail/'. $id); ?>" target="_blank"><img src="http://www.simplesharebuttons.com/images/somacro/google.png" alt="Google" /></a>

    <!-- LinkedIn -->
    <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo site_url('/pressroom/detail/'. $id); ?>" target="_blank"><img src="http://www.simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" /></a>

    <!-- Email -->
    <a href="mailto:?Subject=<?php echo $title; ?>&Body=<?php echo site_url('/pressroom/detail/'. $id); ?>"><img src="http://www.simplesharebuttons.com/images/somacro/email.png" alt="Email" /></a>

    </div>


    <!--<a class="twitter-timeline" href="https://twitter.com/PrimeJobFinder" data-widget-id="397639048111484928">Tweets by @PrimeJobFinder</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    -->

    </div>
	

<?php $this->load->view('_blocks/footer') ?>

