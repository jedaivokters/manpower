<?php $this->load->view('_blocks/header') ?>
<div class="main_inner">
	
	<div class="left" style="width:700px;">
		<div class="user_info left">
			Name: <?php echo $firstname?><?php echo ( $middlename )?' '.$middlename:''?> <?php echo $lastname?><br />
			Address: <?php echo $street_address?><?php echo ($street_address)?' '.$street_address:''?>,
				<?php echo $city?>, <?php echo $province?><?php echo ($zipcode)?', '.$zipcode:''?><br />
			Contact(s): <?php echo $contact_no?><?php echo ( $contact_no2 )?', '.$contact_no2:''?><br />
			Resume:
				<?php if($attachfile):?>
				<a href="<?php echo site_url('fuel/resumes/'.$attachfile)?>" target="_blank">Click to download</a>
				<?php else:?>
				No Resume Uploaded, click the button below
				<?php endif;?>
		</div>

		<div class="clear"><!--
			<button onclick="document.getElementById('resume').click()">Upload Resume</button>
			<form method="post" enctype="multipart/form-data" action="/profile/upload_photo" style="display:none">
			<input type="file" name="resume" id="resume" /><br />
			<input type="submit" />
			</form>-->
		</div>
		
		<br /><br />
		
		
		<h3 class="left" style="margin:0;">Skills</h2>
		<div class="right">
			<input type="button" class="trigger-dialog easyui-linkbutton" data-dialog="add-skill-form" value="Add Skill" />
		</div>
		<div class="clear" style="border-bottom: 2px solid #207bc0;"></div>
		<form class="easyui-dialog ajax" id="add-skill-form" method="post" action="<?php echo site_url('profile/add_skill')?>" title="Add Skill" data-options="iconCls:'icon-save'" data-callback="get_skills" style="padding:20px;width:400px">
			<?php $this->load->view('_blocks/sections/form_profile_skill')?>
		</form>
		<table style="font-size:1em" data-type="get_skills">
			<thead>
				<tr>
					<th>Skill</th>
					<th>Proficiency</th>
					<th>Years</th>
					<th></th>
				</tr>
			</thead>
			<tbody><?php echo $skills?></tbody>
		</table>
		
		<br /><br />

		<h3 class="left" style="margin:0;">Work Experience</h2>
		<div class="right">
			<input type="button" class="trigger-dialog easyui-linkbutton" data-dialog="add-exp-form" value="Add Experience" />
		</div>
		<div class="clear" style="border-bottom: 2px solid #207bc0;"></div>
		<form class="easyui-dialog ajax" id="add-exp-form" method="post" action="<?php echo site_url('profile/add_experience')?>" title="Add Experience" data-options="iconCls:'icon-save'" data-callback="get_experience" style="padding:20px;width:600px">
			<?php $this->load->view('_blocks/sections/form_profile_experience')?>
		</form>
		<table style="font-size:1em" data-type="get_experience">
			<thead>
				<tr>
					<th>Job Title</th>
					<th>Company</th>
					<th>Industry</th>
					<th>Contact Number</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody><?php echo $experiences?></tbody>
		</table>

		<br /><br />

		<h3 class="left" style="margin:0;">Education</h2>
		<div class="right">
			<input type="button" class="trigger-dialog easyui-linkbutton" data-dialog="add-edu-form" value="Add Education" />
		</div>
		<div class="clear" style="border-bottom: 2px solid #207bc0;"></div>
		<form class="easyui-dialog ajax" id="add-edu-form" method="post" action="<?php echo site_url('profile/add_education')?>" title="Add Education" data-options="iconCls:'icon-save'" data-callback="get_education" style="padding:20px;width:300px">
			<?php $this->load->view('_blocks/sections/form_profile_education')?>
		</form>
		<table style="font-size:1em" data-type="get_education">
			<thead>
				<tr>
					<th>Degree</th>
					<th>School</th>
					<th>Location</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody><?php echo $education?></tbody>
		</table>
	
	</div>
	
	<div class="right">&nbsp;</div>
	
	<div class="clear"></div>
    
	
</div>
<br /><br />
<div class="main_inner">
    <h2 style="border-bottom: 4px solid #207bc0;">Recent Applications</h2>
    <div>

        <?php if($applications):?>
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Title</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($applications as $a):?>
                <tr>
                    <td><?php echo $a->date_added?></td>
                    <td><a href="<?php echo site_url('main/details/'.$a->job_post_id)?>" target="_blank"><?php echo $a->title?></a></td>
                    <td>
                        <?php if($a->is_synced):?>
                        Processing
                        <?php else:?>
                        Pending<!--<a href="#">Withdraw Application</a>-->
                        <?php endif;?>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <?php else:?>
        <h3>No recent applications</h3>
        <?php endif;?>
    </div>
</div>
<style type="text/css">
	table {
		border-collapse:collapse;
		width:100%;
	}
	table th, table td {
		border-right:1px solid #fff;
		padding:0 10px;
	}
	table th:last-child, table td:last-child {
		border-right:0;
		text-align:center;
	}
	table thead tr th {
		background: #888;
		color:#fff;
	}
	table tr:nth-child(even) {
		background:#eee;
	}
	.trigger-dialog {
		font-size:1em;
	}
	.btn-tool {
		text-decoration:none;
		color: #207bc0;
	}
	.btn-tool:hover {
		text-decoration:underline;
	}
</style>
<script type="text/javascript">
	jQuery.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true
	});
	jQuery('.date-picker').datepicker();
	
	var preloader = jQuery('<div class="easyui-window" style="min-width:300px;padding:20px" title="Message" />');
	jQuery('body').append(preloader);
	jQuery('.easyui-window').window({
		closed: true,
		modal: true,
		minimizable: false,
		maximizable: false,
		resizable: false,
		collapsible: false,
		closable: false
	});
	jQuery('.easyui-dialog').dialog({
		closed: true,
		modal: true
	});
	jQuery('body').on('click','.trigger-dialog', function(e){
		var form = jQuery(this).attr('data-dialog');
                $('input[name="education_id"]').val('');
                $('input[name="experience_id"]').val('');
		jQuery('#'+form).find('[type="reset"]').click();
		jQuery('#'+form).resetForm();
		jQuery('#'+form).dialog('open');
	});
	jQuery('.easyui-dialog input[type=reset]').on('click',function(){
		jQuery(this).parents('form').dialog('close');
	});
	jQuery('form.ajax').ajaxForm({
		dataType: 'json',
		beforeSubmit: function(data,form) {
			if( !validate_form(form) ) {
				return false;
			}	
			jQuery(form).dialog('close');
			preloader.html('Please wait...');
			preloader.window('open');
		},
		success: function(response,status,xhr,form) {
			preloader.window('close');
			if(response.status == 'success') {
				jQuery('body').trigger(jQuery(form).attr('data-callback'));
			}

			if(typeof response.message != 'undefined') {
				msgprompt.html(response.message);
				msgprompt.window('open');
			}
		}
	});
	jQuery('body').on('get_skills',function(){
		jQuery('body').trigger('get-profile-data',['get_skills']);
	});
	jQuery('body').on('get_experience',function(){
		jQuery('body').trigger('get-profile-data',['get_experience']);
	});
	jQuery('body').on('get_education',function(){
		jQuery('body').trigger('get-profile-data',['get_education']);
	});
	jQuery('body').on('get-profile-data',function(e,type){
		jQuery.ajax({
			url: jQuery('base').attr('href')+'profile/'+type,
			dataType: 'json',
			success: function(response) {
				jQuery('table[data-type="'+type+'"] tbody').html(response.html);
			}
		});
	});
	jQuery('select[name=country_id]').on('change',function(e){
		if( jQuery(this).val() == '163' ) {
			jQuery('select[name=city_id],select[name=province_id]').show();
			jQuery('input[name=city_name],input[name=province_name]').val('').hide();
		} else {
			jQuery('select[name=city_id],select[name=province_id]').val('').hide();
			jQuery('input[name=city_name],input[name=province_name]').show();
		}
	});
	jQuery('body').on('click','.btn-tool.delete',function(e){
		if( !confirm('Delete this record?') )
			return false;
		var me = jQuery(this);
		e.preventDefault();
		jQuery.ajax({
			url: jQuery(this).attr('href'),
			dataType: 'json',
			success: function(response) {
				if(response.status == 'success') {
					me.parents('tr').remove();
				}
			}
		});
	});
	jQuery('body').on('click','.btn-tool.edit-exp',function(e){
		e.preventDefault();
		jQuery('#add-exp-form').find('input[name="experience_id"]').val( jQuery(this).attr('data-experience_id') );
		jQuery('#add-exp-form').find('input[name="job_title"]').val( jQuery(this).attr('data-job_title') );
		jQuery('#add-exp-form').find('input[name="company"]').val( jQuery(this).attr('data-company') );
		jQuery('#add-exp-form').find('input[name="location"]').val( jQuery(this).attr('data-location') );
		jQuery('#add-exp-form').find('[name="industry"]').val( jQuery(this).attr('data-industry') );
		jQuery('#add-exp-form').find('input[name="contact_no"]').val( jQuery(this).attr('data-contact_no') );
		jQuery('#add-exp-form').find('input[name="start_date"]').datepicker('setDate', jQuery(this).attr('data-start_date') );
		jQuery('#add-exp-form').find('input[name="end_date"]').datepicker('setDate', jQuery(this).attr('data-end_date') );
		jQuery('#add-exp-form').find('[name="job_description"]').val( jQuery(this).attr('data-job_description') );
	});
	jQuery('body').on('click','.btn-tool.edit-edu',function(e){
		e.preventDefault();
		jQuery('#add-edu-form').find('[name="education_id"]').val( jQuery(this).attr('data-education_id') );
		jQuery('#add-edu-form').find('[name="degree_id"]').val( jQuery(this).attr('data-degree_id') ).trigger('change');
		jQuery('#add-edu-form').find('[name="degree_others"]').val( jQuery(this).attr('data-degree_others') );
		jQuery('#add-edu-form').find('[name="school"]').val( jQuery(this).attr('data-school') );
		jQuery('#add-edu-form').find('[name="country_id"]').val( jQuery(this).attr('data-country_id') ).trigger('change');
		jQuery('#add-edu-form').find('[name="city_id"]').val( jQuery(this).attr('data-city_id') );
		jQuery('#add-edu-form').find('[name="start_date"]').datepicker('setDate', jQuery(this).attr('data-start_date') );
		jQuery('#add-edu-form').find('[name="end_date"]').datepicker('setDate', jQuery(this).attr('data-end_date') );
		jQuery('#add-edu-form').find('[name="city_name"]').val( jQuery(this).attr('data-city_name') );
		jQuery('#add-edu-form').find('[name="province_id"]').val( jQuery(this).attr('data-province_id') );
		jQuery('#add-edu-form').find('[name="province_name"]').val( jQuery(this).attr('data-province_name') );
		jQuery('#add-edu-form').find('[name="start_date"]').val( jQuery(this).attr('data-start_date') );
	});
	jQuery('body').on('change','[name=degree_id]',function() {

		if( parseInt(jQuery(this).val()) == 0 ) {
			jQuery('[name=degree_others]').parent('div').show();
		} else {
			jQuery('[name=degree_others]').val('');
			jQuery('[name=degree_others]').parent('div').hide();
		}
	});
	jQuery('body').on('change','#add-skill-form [name=skill_id]',function() {

		if( parseInt(jQuery(this).val()) == 0 ) {
			jQuery('#add-skill-form [name=title]').parent('div').show();
		} else {
			jQuery('#add-skill-form [name=title]').val('');
			jQuery('#add-skill-form [name=title]').parent('div').hide();
		}
	});
	
</script>
<?php $this->load->view('_blocks/footer') ?>
