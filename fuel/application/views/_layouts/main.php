<?php $this->load->view('_blocks/header') ?>
<?php if ($has_banner == 'yes') { ?>
    <div class="theme-default">
        <div id="slider" class="nivoSlider">
            <?php
            $slides = fuel_model('slide_shows', array('order' => 'precedence ASC'));
            foreach ($slides as $s) {
                ?>
                <img src="<?php echo assets_path('banners/' . $s->image) ?>"  title="<?php echo $s->title; ?>" />
            <?php } ?>
        </div>
    </div>

    <script>
        $('#slider').nivoSlider();
    </script>
<?php } ?>

<?php if (isset($view_job_details)) {
    ?>
    <div class="main_inner">
        <h2><?php echo $j_job_title; ?></h2>
	
	
	<br />
         <?php echo $j_content; ?>
	<br />
	<div>
		<label>Address: </label><?php echo $j_address .' ' . $j_city .','. $j_zipcode; ?><br />
		<label>Contact Info: </label><?php echo $j_contact_no; ?><br />
		<label>Salary: </label><?php echo $j_salary; ?><br />
		<label>Occupation Type: </label><?php //echo $j_salary; ?><br />
		<label>Date Available: </label><?php echo date("F j, Y", strtotime($j_from_date)) . ' to ' . date("F j, Y", strtotime($j_to_date)); ?><br />
	</div>
	<br />
		<a href="<?php echo site_url('profile/apply/'.$j_id)?>" class="btn blue big apply">Apply Now!</a>
        <div id="apply_panel" class="easyui-accordion" style="display:none">
            <div title="Apply Now!" style="padding:10px;">
                <form id="apply_form" class="form" action ="<?php echo site_url('main/apply') ?>" enctype='multipart/form-data'>
                    <input type ="hidden" name="id" id="id" value ="<?php echo $j_id; ?>" />
                    <div class="left" style="width: 428px;">
                        <div>
                            <label>
                                <span class="required">*</span>First Name:
                            </label>
                            <div class="left">
                                <input type="text" name="fname" id="fname" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>Middle Name:</label>
                            <div class="left" >
                                <input type="text" name="mname" id="mname" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>
                                <span class="required">*</span>Last Name:
                            </label>
                            <div class="left">
                                <input type="text" name="lname" id="lname" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>
                                <span class="required">*</span>E-mail:
                            </label>
                            <div class="left">
                                <input type="text" name="email" id="email" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>
                                <span class="required">*</span>Attach resume:
                            </label>
                            <div class="left">
                                <input type="file" name="resume" id="resume">
				<br />
				<i>Allowed file type: .docx, .pdf, .odt, .doc, .txt </i>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <!-- Column 2 -->
                    <div class="left" style="width: 470px;">
                        <div>
                            <label>
                                <span class="required">*</span>Street Address:
                            </label>
                            <div class="left">
                                <input type="text" name="saddress" id="saddress" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>Street Address #2:</label>
                            <div class="left" >
                                <input type="text" name="saddress2" id="saddress2" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>
                                <span class="required">*</span>City:
                            </label>
                            <div class="left">
                                <input type="text" name="city" id="city" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>
                                Province:
                            </label>
                            <div class="left">
                                <input type="text" name="province" id="province" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>
                                <span class="required">*</span>Zip Code:
                            </label>
                            <div class="left">
                                <input type="text" name="zipcode" id="zipcode">
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>
                                <span class="required">*</span>Contact Info:
                            </label>
                            <div class="left">
                                <input type="text" name="contact" id="contact">
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <label>
                                <span class="required"></span>Contact Info #2:
                            </label>
                            <div class="left">
                                <input type="text" name="contact2" id="contact2">
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>


                    <div class="clear"></div>
                    <br />
                    <a id="apply_form_submit_btn" href="#" class="easyui-linkbutton">Submit</a>
                </form>
                <div class="" id="messager" style="">
                    <div style="width: 256px;margin:0 auto">
                        <span id="note">Loading....</span>
                        <div id="p" style="width: 243px;" class="hide"></div>
                        <div style="width: 100%;margin:0 auto;margin-top: 5px;" align="center">
                            <a id="message_close_btn" class="easyui-linkbutton hide" href="#" class="hide">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#apply_panel').accordion();
            var p = $('#apply_panel').accordion('getSelected');
            //work around click to toogle collapse
            $(p).prev().trigger('click');

            $('#messager').window({
                title: 'Submitting form...',
                width: 400,
                height: 110,
                closed: true,
                modal: true,
                maximizable: false,
                minimizable: false,
                resizable: false,
                closable: false,
                collapsible: false
            });

            $('#p').progressbar({
                value: 0
            });

            $('#saddress').bind('keyup', function() {
                name = $(this).val();
                //$('#saddress2').val(name);
            });

            $('#contact').bind('keyup', function() {
                name = $(this).val();
                //$('#contact2').val(name);
            });

            $('#apply_form_submit_btn').click(function(e) {
                e.preventDefault();

                $('#p').progressbar('setValue', 0);
                $(window).scrollTop(0);
                $('#messager').window('open');
                $('#message_close_btn').removeClass('l-btn');

                //Check if progress bar supported
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    //Unhide progress bar
                    $('#p').removeClass('hide');
                }

                $('#apply_form').ajaxSubmit({
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    type: 'POST',
                    success: function(response, status) {
                        $('#p').progressbar('setValue', 100);

                        if (!response.success) {
                            $(window).scrollTop($(window).height());
                            $('#messager').window('close');
                            process_error(response)
                            return;
                        }

                        process_error(response);
                        //Unhide
                        $('#note').html('Your request is successfully submitted.');
                        $('#message_close_btn').removeClass('hide');
                        $('#message_close_btn').addClass('l-btn');
                        $('#message_close_btn').click(function(e) {
                            e.preventDefault();
                            window.location = '<?php echo site_url() ?>';
                        });

                        return;
                    },
                    uploadProgress: function(e, pos, t, p) {
                        if (p < 95) {
                            $('#p').progressbar('setValue', p);
                        }
                    }
                });
            });


            function process_error(response) {
                for (var e in response.errors) {
                    if (response.errors[e] != '') {
                        $('#' + e).tooltip({
                            position: 'right',
                            content: response.errors[e],
                            onShow: function() {
                                $(this).tooltip('tip').css({
                                    borderColor: 'red'
                                });
                            }
                        });

                        $('#' + e).addClass('error_border')
                    }

                    //Remove any error message if theres any
                    if (response.errors[e] == '') {
                        $('#' + e).tooltip();
                        $('#' + e).tooltip('destroy');
                        $('#' + e).removeClass('error_border');
                    }
                }
            }
        </script>
        <div class="clear"></div>
    </div>
<?php } ?>


<?php if (!empty($requisition_section)) {
    ?>
    <?php if (!empty($requisition_section[0]['content2'])) {
        ?>
        <br />
        <div class="main_inner">
            <div style="">
                <?php foreach ($requisition_section as $section) {
                    ?>
                    <?php
                    echo fuel_block('_requisition', array(
                        'content' => $section['content2'],
                        'image_left' => $section['image_left2'],
                    ));
                    ?>
                <?php } ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div id ="content_recruit" style="overflow-y: auto;height: 150px;"></div>
            <script>
                $('.recruitment').mouseenter(function() {
                    var c = $(this).next().val();
                    $('#content_recruit').html(c);
                });
            </script>
        </div>
    <?php } ?>
<?php } ?>


<br />
<?php if (!empty($body)) {
    ?>
    <div id="main_inner">
        <?php echo fuel_var('body', ''); ?>
        <div class="clear"></div>
    </div>
	<br />
<?php } ?>

<?php if (!empty($news) && $news == 'yes') { ?>
    <div>
        <div>
            <?php
            $news = fuel_model('news', array('where'=> array('news_type_id' => $news_type), 'order' => 'id DESC'));
            foreach ($news as $n) {
                ?>
                <div class="main_inner">
                    <?php if (!empty($n->image)){ ?>
                    <div class="imgThumbnail nailthumb-container th-100x60"><a href="<?php echo site_url('pressroom/detail/'. $n->id); ?>"><img border="0" style="width: 100px;height: 60px;" src="<?php echo assets_path('images/' . $n->image) ?>"  /></a></div>
                    <?php } ?>
                    <div class="site-area-list">
                    <h3><a target="" title="" href="<?php echo site_url('pressroom/detail/'. $n->id); ?>" ><?php echo $n->title; ?></a></h3>
                    <p><?php echo word_limiter($n->content, 50, 3); ?>&nbsp;<a target="" title="" href="<?php echo site_url('pressroom/detail/'. $n->id); ?>">&nbsp;&nbsp;Read&nbsp;&raquo;</a></p>
                    </div>
                    <div class="clear"></div>
                </div>
                <br />
            <?php } ?>
        </div>
    </div>
<?php } ?>


<?php if (!empty($job_search_main) && $job_search_main == 'yes') {
    ?>
	<br />
    <div id="job_search_main">
        <h2><?php echo ($job_search_type == 'all') ? '' : $job_search_type . ' - '; ?>Job Search</h2>
        <?php if (!empty($job_search_icon) && $job_search_icon == 'yes') { ?>
        <div style="border-top: 5px solid #207bc0;height: 5px;"></div>
            <div  style="" class="row" align="right">
                <?php
                if ($job_search_type == 'all') {
                    $positions = fuel_model('positions', array('published' => 'yes'));
                } else {
                    $positions = fuel_model('positions', 'all', array('published' => 'yes', 'search_type' => $job_search_type));
                }
                foreach ($positions as $p) {
                    if (!empty($p->image)) {
                        ?>
                        <a href ="#" class="position_type" position_name ="<?php echo $p->name ?>">
                            <img src="<?php echo assets_path('positions/' . $p->image) ?>"  title="<?php echo $p->name; ?>" />
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        <?php } ?>
        <div class="row hide">
            <form id="search_form_advanced">
                <label>Keywords</label>
                <input type="text" name="keywords" id ="keywords" title="Job Title Or Position" class="easyui-tooltip" />
                <a href="#" id="job_search_btn" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
                <input type="submit" style="position:absolute;top:-100px"/>
            </form>
        </div>
        <br />
        
        <div class="row" id="job_container"></div>
        
        <table class="hide" id="job_table" title="Search" style="height:320px"></table>

        <script>
            var details_url = '<?php echo site_url('main/details'); ?>';
            var job_search_url = '<?php echo site_url('job/search'); ?>';


            $('.position_type').click(function(e) {
                e.preventDefault();
                $('#keywords').val($(this).attr('position_name'));
                $('#job_search_btn').click();
            });

            $('#job_search_btn').click(function(e) {
                e.preventDefault();
                var search = {
                    keywords: $('#keywords').val(),
                    search_type: '<?php echo $job_search_type; ?>'
                }
                
                //$('#job_container').load(job_search_url, search);
                $('#job_table').datagrid('load', {
                    keywords: $('#keywords').val(),
                    search_type: '<?php echo $job_search_type; ?>'
                });
            });

            $('#search_form_advanced').submit(function(e) {
                e.preventDefault();
                $('#job_search_btn').trigger('click');
            });

    <?php if (isset($search_top)) { ?>
                $('#keywords').val('<?php echo $q; ?>');
    <?php } ?>
            var search = {
                keywords: $('#keywords').val(),
                search_type: '<?php echo $job_search_type; ?>'
            }
            //$('#job_container').load(job_search_url, search);
            $('#job_table').datagrid({
                queryParams: {
                    keywords: $('#keywords').val(),
                    search_type: '<?php echo $job_search_type; ?>'
                },
                noheader: true,
                url: job_search_url,
                idField: 'id',
                pagination: true,
                method: 'post',
                singleSelect: true,
                columns: [[
                        {field: 'id', title: 'Code', width: 100, hidden: true},
                        {field: 'job_title', title: 'Job Title', width: 550, sortable: true},
                        {field: 'city', title: 'Location', width: 120, align: 'center'},
                        {field: 'occupation_type', title: 'Type', width: 90, align: 'center'},
                        {field: 'action', title: 'Action', width: 130, align: 'center', formatter: function(value, row, index) {
                                return '<a target="_blank" href="' + details_url + '/'
                                        + row.id + '" class="apply_btn">View Details</a>';
                            }}
                    ]],
                onLoadSuccess: function(data) {
                    $('.apply_btn').linkbutton();
                }
            });


        </script>
    </div>
<?php } ?>

<?php if (!empty($contact_form) && $contact_form == 'yes') {
    ?>
    <div class="main_inner">
        <h2>Contact Us</h2>
        <br />
        <h2 style="color:#2db6df;margin: 0px">How can we help you?</h2>
        <p style="color:#666">Prime Manpower welcomes your inquiries, comments, suggestions and questions.</p>
        <br />
        <div class="easyui-tabs" id="contact-tab-wrapper">
            <div style="padding: 10px" title="Employers" id="emp-tab">
                <div class="left" style="width: 500px;">
                    <?php if (isset($employer_body))
                        echo $employer_body;
                    ?>
                    <div id ="contact_form_emp_wrapper" style="padding:10px;">
                        <form id="contact_form_emp" class="form" action ="<?php echo site_url('main/contact') ?>" enctype='multipart/form-data'>
                            <div class="left" style="width: 428px;">
                                <div>
                                    <label>
                                        <span class="required">*</span>First Name:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="ce_fname" id="ce_fname" />
                                        <input type="hidden" name="ce" id="ce" value="1" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>Last Name:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="ce_lname" id="ce_lname" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>Title:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="ce_title" id="ce_title" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>Company E-mail:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="ce_email" id="ce_email" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>Company:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="ce_company" id="ce_company" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>Phone:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="ce_phone" id="ce_phone" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        How can we help you?:
                                    </label>
                                    <div class="left">
                                        <textarea id ="ce_message" name="ce_message"></textarea>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <br />
                            <a id="contact_form_emp_submit_btn" href="#" class="easyui-linkbutton">Submit</a>
                        </form>
                    </div>
                    <a href="#" class="contact_form_emp_contact_btn easyui-linkbutton">Contact Us</a>
                </div>
                <div class="right" style="width: 250px; padding: 10px;border-width: 1px;border-style: solid; border-color: #95B8E7;">
                    <?php if (isset($employer_side))
                        echo $employer_side;
                    ?>
                    <br />
                    <a href="#" class="contact_form_emp_contact_btn easyui-linkbutton">Contact Us</a>
                </div>
            </div>
            <div title="Jobseekers" style="padding: 10px" id="job-tab" >
                <div class="left" style="width: 500px;">
    <?php if (isset($job_seekerbody))
        echo $job_seekerbody;
    ?>
                    <div id="contact_form_job_wrapper" style="padding: 10px;">
                        <form id="contact_form_job" class="form" action ="<?php echo site_url('main/contact') ?>" enctype='multipart/form-data'>
                            <div class="left" style="width: 428px;">
                                <div>
                                    <label>
                                        <span class="required">*</span>First Name:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="c_fname" id="c_fname" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>Last Name:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="c_lname" id="c_lname" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>Position applied for:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="c_title" id="c_title" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>E-mail:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="c_email" id="c_email" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        <span class="required">*</span>Phone:
                                    </label>
                                    <div class="left">
                                        <input type="text" name="c_phone" id="c_phone" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        Attach resume:
                                    </label>
                                    <div class="left">
                                        <input type="file" name="resume" id="resume">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>
                                    <label>
                                        How can we help you?:
                                    </label>
                                    <div class="left">
                                        <textarea id ="c_message" name="c_message"></textarea>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="clear"></div>
                            <br />
                            <a id="contact_form_job_submit_btn" href="#" class="easyui-linkbutton">Submit</a>
                        </form>
                    </div>
                    <a href="#" class="contact_form_job_contact_btn easyui-linkbutton">Contact Us</a>
                </div>
                <div class="right" style="width: 250px; padding: 10px;border-width: 1px;border-style: solid; border-color: #95B8E7;">
    <?php if (isset($job_seekerside)) echo $job_seekerside; ?>
                    <br />
                    <a href="#" class="contact_form_job_contact_btn easyui-linkbutton">Contact Us</a>
                </div>
            </div>
        </div>

        <div class="" id="messager" style="">
            <div style="width: 256px;margin:0 auto">
                <span id="note">Loading....</span>
                <div id="p" style="width: 243px;" class="hide"></div>
                <div style="width: 100%;margin:0 auto;margin-top: 5px;" align="center">
                    <a id="message_close_btn" class="easyui-linkbutton hide" href="#" class="hide">Close</a>
                </div>
            </div>
        </div>

        <div class="clear"></div>
    </div>

    <script>
        //Need to ready document in order to apply changes in DOM
        $(document).ready(function() {
            //$('.tabs > li').css({cursor: 'pointer'});
            employer_tab = $('.tabs > li')[0];
            jobseeker_tab = $('.tabs > li')[1];

            $(employer_tab).mouseenter(function() {
                $(this).click();
            });
            $(jobseeker_tab).mouseenter(function() {
                $(this).click();
            });
        });

        //Employers
        $('.contact_form_emp_contact_btn').click(function(e) {
            e.preventDefault();
            $('#contact_form_emp_wrapper').window('open');
        });
        //Jobseeker
        $('.contact_form_job_contact_btn').click(function(e) {
            e.preventDefault();
            $('#contact_form_job_wrapper').window('open');
        });
        //Employers
        $('#contact_form_emp_wrapper').window({
            title: 'Employer Contact Form',
            closed: true,
            modal: true,
            maximizable: false,
            minimizable: false,
            resizable: false,
            closable: true,
            collapsible: false,
            onClose: function() {
                //Remove Error
                var inputs = $("input[id^=ce_]");
                for (var i = 0; i < inputs.length; i++) {
                    $(inputs[i]).tooltip();
                    $(inputs[i]).tooltip('destroy');
                    $(inputs[i]).removeClass('error_border');
                    //console.debug();
                }
            }
        });
        //Job Seeker
        $('#contact_form_job_wrapper').window({
            title: 'JobSeeker Contact Form',
            closed: true,
            modal: true,
            maximizable: false,
            minimizable: false,
            resizable: false,
            closable: true,
            collapsible: false,
            onClose: function() {
                //Remove Error
                var inputs = $("input[id^=c_]");
                for (var i = 0; i < inputs.length; i++) {
                    $(inputs[i]).tooltip();
                    $(inputs[i]).tooltip('destroy');
                    $(inputs[i]).removeClass('error_border');
                }
            }
        });

        $('#messager').window({
            title: 'Submitting form...',
            width: 400,
            height: 110,
            closed: true,
            modal: true,
            maximizable: false,
            minimizable: false,
            resizable: false,
            closable: false,
            collapsible: false
        });

        $('#p').progressbar({
            value: 0
        });

        $('#contact_form_emp_submit_btn').click(function(e) {
            e.preventDefault();

            $('#p').progressbar('setValue', 0);
            $(window).scrollTop(0);
            $('#messager').window('open');
            $('#message_close_btn').removeClass('l-btn');

            //Check if progress bar supported
            myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                //Unhide progress bar
                $('#p').removeClass('hide');
            }

            $('#contact_form_emp').ajaxSubmit({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                type: 'POST',
                success: function(response, status) {
                    $('#p').progressbar('setValue', 100);

                    if (!response.success) {
                        //$(window).scrollTop($(window).height());
                        $('#messager').window('close');
                        process_error(response)
                        return;
                    }

                    process_error(response);
                    //Unhide

                    var msg = 'Your request is successfully submitted.';
                    if (!response.sent) {
                        msg = 'An error occur. Pls try again.'
                    }

                    $('#note').html(msg);
                    $('#message_close_btn').removeClass('hide');
                    $('#message_close_btn').addClass('l-btn');
                    $('#message_close_btn').click(function(e) {
                        e.preventDefault();
                        location.reload();
                    });

                    return;
                },
                uploadProgress: function(e, pos, t, p) {
                    if (p < 95) {
                        $('#p').progressbar('setValue', p);
                    }
                }
            });
        });

        $('#contact_form_job_submit_btn').click(function(e) {
            e.preventDefault();

            $('#p').progressbar('setValue', 0);
            $(window).scrollTop(0);
            $('#messager').window('open');
            $('#message_close_btn').removeClass('l-btn');

            //Check if progress bar supported
            myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                //Unhide progress bar
                $('#p').removeClass('hide');
            }

            $('#contact_form_job').ajaxSubmit({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                type: 'POST',
                success: function(response, status) {
                    $('#p').progressbar('setValue', 100);

                    if (!response.success) {
                        //$(window).scrollTop($(window).height());
                        $('#messager').window('close');
                        process_error(response)
                        return;
                    }

                    process_error(response);
                    //Unhide

                    var msg = 'Your request is successfully submitted.';
                    if (!response.sent) {
                        msg = 'An error occur. Pls try again.'
                    }

                    $('#note').html(msg);
                    $('#message_close_btn').removeClass('hide');
                    $('#message_close_btn').addClass('l-btn');
                    $('#message_close_btn').click(function(e) {
                        e.preventDefault();
                        location.reload();
                    });

                    return;
                },
                uploadProgress: function(e, pos, t, p) {
                    if (p < 95) {
                        $('#p').progressbar('setValue', p);
                    }
                }
            });
        });

        function process_error(response) {
            for (var e in response.errors) {
                if (response.errors[e] != '') {
                    $('#' + e).tooltip({
                        position: 'right',
                        content: response.errors[e],
                        onShow: function() {
                            $(this).tooltip('tip').css({
                                borderColor: 'red'
                            });
                        }
                    });

                    $('#' + e).addClass('error_border')
                }

                //Remove any error message if theres any
                if (response.errors[e] == '') {
                    $('#' + e).tooltip();
                    $('#' + e).tooltip('destroy');
                    $('#' + e).removeClass('error_border');
                }
            }
        }
    </script>
<?php } ?>


<?php if (!empty($bottom_middle_section)) {
    ?>
        <?php if (!empty($bottom_middle_section[0]['title'])) {
            ?>
        <div id="bottom_middle_seciton">
            <?php
            foreach ($bottom_middle_section as $section) :
                $block_name = $section['block'];
                if (!empty($block_name)) :
                    ?>
                    <?php
                    echo fuel_block('sections/' . $block_name, array(
                        'content' => $section['content'],
                        'image_left' => $section['image_left'],
                        'title' => $section['title']
                    ))
                    ?>
            <?php endif; ?>
        <?php endforeach; ?>
            <div class="clear"></div>
        </div>
    <?php } ?>
<?php } ?>


<?php $this->load->view('_blocks/footer') ?>
