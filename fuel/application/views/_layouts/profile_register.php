<?php $this->load->view('_blocks/header') ?>
<div class="main_inner">

    <?php if( $this->input->get('status') == 'registered' ):?>
    <h2>Success!</h2>
    <p>Thank you for registering.</p>
    <p>Please check your email to activate your account.</p><br />
    <p>Enter the code from your email address to activate your account.</p><br />
    <form method="post" enctype="multipart/form-data" action="<?php echo site_url('profile/activate')?>">
        <input type="hidden" name="hash" id="hash" value="<?php echo $hash?>" />
        <input type="text" name="activationcode" id="activationcode" placeholder="Enter Activation Code" />
        <input type="submit" value="Submit" />
    </form>
    <?php else:?>
    <div class="left">
    <h2>Please Register</h2>

    <form method="post" enctype="multipart/form-data" action="<?php echo site_url('profile/register')?>">
        <ul class="field-set">
            <li class="form-field firstname required">
                <label for="firstname">First Name</label>
                <div class="input"><input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname');?>" /></div>
                <?php echo form_error('firstname'); ?>
            </li>
            <li class="form-field middlename">
                <label for="middlename">Middle Name</label>
                <div class="input"><input type="text" name="middlename" id="middlename" value="<?php echo set_value('middlename');?>" /></div>
                <?php echo form_error('middlename'); ?>
            </li>
            <li class="form-field lastname required">
                <label for="lastname">Last Name</label>
                <div class="input"><input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname');?>" /></div>
                <?php echo form_error('lastname'); ?>
            </li>
            <li class="form-field email_address required">
                <label for="email_address">Email</label>
                <div class="input"><input type="text" name="email_address" id="email_address" value="<?php echo set_value('email_address');?>" /></div>
                <?php echo form_error('email_address'); ?>
            </li>
            <li class="form-field password required">
                <label for="password">Password</label>
                <div class="input">
                    <input type="password" name="password" id="password" placeholder="Desired Password" /><br />
                    <input type="password" name="password2" id="password2" placeholder="Confirm Password" />
                </div>
                <?php echo form_error('password'); ?>
            </li>
        </ul>
        <ul class="field-set">
            <li class="form-field street_address required">
                <label for="street_address">Stree Address</label>
                <div class="input"><input type="text" name="street_address" id="street_address" class="long" value="<?php echo set_value('street_address');?>" /></div>
                <?php echo form_error('street_address'); ?>
            </li>
            <li class="form-field street_address2">
                <label for="street_address">Street Address #2</label>
                <div class="input"><input type="text" name="street_address2" id="street_address2" class="long" value="<?php echo set_value('street_address2');?>" /></div>
                <?php echo form_error('street_address2'); ?>
            </li>
            <li class="form-field city required">
                <label for="city">City</label>
                <div class="input"><input type="text" name="city" id="city" class="long" value="<?php echo set_value('city');?>" /></div>
                <?php echo form_error('city'); ?>
            </li>
            <li class="form-field province">
                <label for="province">Province</label>
                <div class="input"><input type="text" name="province" id="province" class="long" value="<?php echo set_value('province');?>" /></div>
                <?php echo form_error('province'); ?>
            </li>
            <li class="form-field zipcode required">
                <label for="zipcode">Zip Code</label>
                <div class="input"><input type="text" name="zipcode" id="zipcode" value="<?php echo set_value('zipcode');?>" /></div>
                <?php echo form_error('username'); ?>
            </li>
            <li class="form-field contact_no required">
                <label for="contact_no">Contact Info</label>
                <div class="input"><input type="text" name="contact_no" id="contact_no" value="<?php echo set_value('contact_no');?>" /></div>
                <?php echo form_error('contact_no'); ?>
            </li>
            <li class="form-field contact_no2">
                <label for="contact_no2">Contact Info #2</label>
                <div class="input"><input type="text" name="contact_no2" id="contact_no2" value="<?php echo set_value('contact_no2');?>" /></div>
                <?php echo form_error('contact_no2'); ?>
            </li>
            <li class="form-field attachfile">
                <label for="attachfile">Attach Resume</label>
                <div class="input"><input type="file" name="attachfile" id="attachfile" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" value="<?php echo set_value('contact_no2');?>"  /></div>
                <?php echo form_error('attachfile'); ?>
            </li>
        </ul>
        <div class="action-set">
            <input type="submit" value="Submit" />
        </div>
    </form>
    </div>
    <div class="right">
        <h3>Already Registered?</h3>
        <p><a href="<?php echo site_url('profile/login')?>">Click here to login.</a></p>
    </div>

    <div class="clear"></div>
    <?php endif;?>
<style type="text/css">
    .field-set {
        padding:0;
        margin:0;
        list-style-type:none;
    }
    .field-set, .action-set {
        margin-top:20px;
    }

    .field-set .form-field {
        margin-top:10px;
    }
    .field-set .form-field:first-child {
        margin-top:0;
    }
    .field-set .form-field:after {
        content: " ";
        display:block;
        clear:both;
    }
    .field-set label {
        float:left;
        min-width:160px;
        font-size:1.1em;
    }
    .field-set .input {
        float:left;
    }
    .field-set .error {
        float:left;
        margin-left:5px;
    }
    .field-set .form-field .long {
        width:320px;
    }
    .field-set .form-field.required label:after {
        content: " *";
        color:#c00;
    }
</style>
<?php $this->load->view('_blocks/footer') ?>
