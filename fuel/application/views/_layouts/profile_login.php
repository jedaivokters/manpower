<?php $this->load->view('_blocks/header') ?>
<div class="main_inner">

    <h2>Member Login</h2>

    <form method="post">
        <ul class="field-set">
            <li class="form-field firstname required">
                <label for="email_address">Email Address</label>
                <div class="input"><input type="text" name="email_address" id="email_address" value="<?php echo set_value('email_address');?>" /></div>
                <?php echo form_error('email_address'); ?>
            </li>
            <li class="form-field middlename">
                <label for="password">Password</label>
                <div class="input"><input type="password" name="password" id="password" /></div>
                <?php echo form_error('password'); ?>
            </li>
        </ul>
        <div class="action-set">
            <input type="submit" value="Submit" />
            &nbsp;<a href="<?php echo site_url('profile/register')?>">Register</a>
            <!--&nbsp;<a href="<?php echo site_url('profile/reset_password')?>">Forgot Password</a>-->
        </div>
    </form>
<style type="text/css">
    .field-set {
        padding:0;
        margin:0;
        list-style-type:none;
    }
    .field-set, .action-set {
        margin-top:20px;
    }

    .field-set .form-field {
        margin-top:10px;
    }
    .field-set .form-field:first-child {
        margin-top:0;
    }
    .field-set .form-field:after {
        content: " ";
        display:block;
        clear:both;
    }
    .field-set label {
        float:left;
        min-width:160px;
        font-size:1.1em;
    }
    .field-set .input {
        float:left;
    }
    .field-set .error {
        float:left;
        margin-left:5px;
    }
    .field-set .form-field .long {
        width:320px;
    }
    .field-set .form-field.required label:after {
        content: " *";
        color:#c00;
    }
</style>
<?php $this->load->view('_blocks/footer') ?>
