<?php $this->load->view('_blocks/header') ?>
<div class="main_inner">

    <div class="left">
        <h2>Edit Profile</h2>

        <form method="post" enctype="multipart/form-data">
            <ul class="field-set">
                <li class="form-field firstname required">
                    <label for="firstname">First Name</label>
                    <div class="input"><input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname',$firstname);?>" /></div>
                    <?php echo form_error('firstname'); ?>
                </li>
                <li class="form-field middlename">
                    <label for="middlename">Middle Name</label>
                    <div class="input"><input type="text" name="middlename" id="middlename" value="<?php echo set_value('middlename',$middlename);?>" /></div>
                    <?php echo form_error('middlename'); ?>
                </li>
                <li class="form-field lastname required">
                    <label for="lastname">Last Name</label>
                    <div class="input"><input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname',$lastname);?>" /></div>
                    <?php echo form_error('lastname'); ?>
                </li>
            </ul>
            <ul class="field-set">
                <li class="form-field street_address required">
                    <label for="street_address">Stree Address</label>
                    <div class="input"><input type="text" name="street_address" id="street_address" class="long" value="<?php echo set_value('street_address',$street_address);?>" /></div>
                    <?php echo form_error('street_address'); ?>
                </li>
                <li class="form-field street_address2">
                    <label for="street_address">Street Address #2</label>
                    <div class="input"><input type="text" name="street_address2" id="street_address2" class="long" value="<?php echo set_value('street_address2',$street_address2);?>" /></div>
                    <?php echo form_error('street_address2'); ?>
                </li>
                <li class="form-field city required">
                    <label for="city">City</label>
                    <div class="input"><input type="text" name="city" id="city" class="long" value="<?php echo set_value('city',$city);?>" /></div>
                    <?php echo form_error('city'); ?>
                </li>
                <li class="form-field province">
                    <label for="province">Province</label>
                    <div class="input"><input type="text" name="province" id="province" class="long" value="<?php echo set_value('province',$province);?>" /></div>
                    <?php echo form_error('province'); ?>
                </li>
                <li class="form-field zipcode required">
                    <label for="zipcode">Zip Code</label>
                    <div class="input"><input type="text" name="zipcode" id="zipcode" value="<?php echo set_value('zipcode',$zipcode);?>" /></div>
                    <?php echo form_error('username'); ?>
                </li>
                <li class="form-field contact_no required">
                    <label for="contact_no">Contact Info</label>
                    <div class="input"><input type="text" name="contact_no" id="contact_no" value="<?php echo set_value('contact_no',$contact_no);?>" /></div>
                    <?php echo form_error('contact_no'); ?>
                </li>
                <li class="form-field contact_no2">
                    <label for="contact_no2">Contact Info #2</label>
                    <div class="input"><input type="text" name="contact_no2" id="contact_no2" value="<?php echo set_value('contact_no2',$contact_no2);?>" /></div>
                    <?php echo form_error('contact_no2'); ?>
                </li>
                <li class="form-field attachfile">
                    <label for="attachfile">Attach Resume</label>
                    <div class="input"><input type="file" name="attachfile" id="attachfile" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" value="<?php echo set_value('contact_no2');?>"  /></div>
                    <?php echo form_error('attachfile'); ?>
                </li>
            </ul>

            <h3>Job Preferences</h3>

            <ul class="field-set">
                <li class="form-field jp_position_type">
                    <label for="jp_position_type">Position Type</label>
                    <div class="input">
                        <select name="jp_position_type" id="jp_position_type">
                            <?php foreach($list_jp_position_type as $i):?>
                            <option value="<?php echo substr($i,'1','-1')?>" <?php echo set_select('jp_position_type', substr($i,'1','-1'), ($jp_position_type==substr($i,'1','-1'))?TRUE:FALSE); ?>><?php echo substr($i,'1','-1')?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_position_type'); ?>
                </li>
                <li class="form-field jp_prefered_location">
                    <label for="jp_prefered_location">Prefered Location</label>
                    <div class="input">
                        <select name="jp_prefered_location" id="jp_prefered_location">
                            <?php foreach($list_jp_prefered_location as $i):?>
                            <option value="<?php echo substr($i,'1','-1')?>" <?php echo set_select('jp_prefered_location', substr($i,'1','-1'), ($jp_prefered_location==substr($i,'1','-1'))?TRUE:FALSE); ?>><?php echo substr($i,'1','-1')?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_prefered_location'); ?>
                </li>
                <li class="form-field jp_prefered_areas_abroad">
                    <label for="jp_prefered_areas_abroad">Prefered areas abroad</label>
                    <div class="input">
                        <select name="jp_prefered_areas_abroad" id="jp_prefered_areas_abroad">
                            <?php foreach($list_jp_prefered_areas_abroad as $i):?>
                            <option value="<?php echo $i?>" <?php echo set_select('jp_prefered_areas_abroad', $i, ($jp_prefered_areas_abroad==$i)?TRUE:FALSE); ?>><?php echo $i?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_prefered_areas_abroad'); ?>
                </li>
                <li class="form-field jp_prefered_areas_local">
                    <label for="jp_prefered_areas_local">Prefered local areas</label>
                    <div class="input">
                        <select name="jp_prefered_areas_local" id="jp_prefered_areas_local">
                            <?php foreach($list_jp_prefered_areas_local as $i=>$j):?>
                            <?php if(is_array($j)):?>
                            <optgroup label="<?php echo $i;?>">
                                <?php foreach($j as $k):?>
                                <option value="<?php echo $k?>" <?php echo set_select('jp_prefered_areas_local', $k, ($jp_prefered_areas_local==$k)?TRUE:FALSE); ?>><?php echo $k?></option>
                                <?php endforeach;?>
                            </optgroup>
                            <?php else:?>
                            <option value="<?php echo $j?>" <?php echo set_select('jp_prefered_areas_local', $j, ($jp_prefered_areas_local==$j)?TRUE:FALSE); ?>><?php echo $j?></option>
                            <?php endif;?>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_prefered_areas_local'); ?>
                </li>
                <li class="form-field jp_relocate_philippines">
                    <label for="jp_relocate_philippines">Relocate Philippines</label>
                    <div class="input">
                        <select name="jp_relocate_philippines" id="jp_relocate_philippines">
                            <?php foreach($list_jp_relocate_philippines as $i):?>
                            <option value="<?php echo substr($i,'1','-1')?>" <?php echo set_select('jp_relocate_philippines', substr($i,'1','-1'), ($jp_relocate_philippines==substr($i,'1','-1'))?TRUE:FALSE); ?>><?php echo substr($i,'1','-1')?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_relocate_philippines'); ?>
                </li>
                <li class="form-field jp_relocate_abroad">
                    <label for="jp_relocate_abroad">Relocate Abroad</label>
                    <div class="input">
                        <select name="jp_relocate_abroad" id="jp_relocate_abroad">
                            <?php foreach($list_jp_relocate_abroad as $i):?>
                            <option value="<?php echo substr($i,'1','-1')?>" <?php echo set_select('jp_relocate_abroad', substr($i,'1','-1'), ($jp_relocate_abroad==substr($i,'1','-1'))?TRUE:FALSE); ?>><?php echo substr($i,'1','-1')?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_relocate_abroad'); ?>
                </li>
                <li class="form-field jp_industry_id_a">
                    <label for="jp_industry_id_a">Prefered Industry A</label>
                    <div class="input">
                        <select name="jp_industry_id_a" id="jp_industry_id_a">
                            <option value="">- Select One -</option>
                            <?php foreach($list_jp_industry as $i):?>
                            <option value="<?php echo $i->industry_id?>" <?php echo set_select('jp_industry_id_a', $i->industry_id, ($jp_industry_id_a==$i->industry_id)?TRUE:FALSE); ?>><?php echo $i->industry_name?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_industry_id_a'); ?>
                </li>
                <li class="form-field jp_industry_id_b">
                    <label for="jp_industry_id_b">Prefered Industry B</label>
                    <div class="input">
                        <select name="jp_industry_id_b" id="jp_industry_id_b">
                            <option value="">- Select One -</option>
                            <?php foreach($list_jp_industry as $i):?>
                            <option value="<?php echo $i->industry_id?>" <?php echo set_select('jp_industry_id_b',  $i->industry_id, ($jp_industry_id_b==$i->industry_id)?TRUE:FALSE); ?>><?php echo $i->industry_name?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_industry_id_b'); ?>
                </li>
                <li class="form-field jp_industry_id_c">
                    <label for="jp_industry_id_c">Prefered Industry C</label>
                    <div class="input">
                        <select name="jp_industry_id_c" id="jp_industry_id_c">
                            <option value="">- Select One -</option>
                            <?php foreach($list_jp_industry as $i):?>
                            <option value="<?php echo $i->industry_id?>" <?php echo set_select('jp_industry_id_c',  $i->industry_id, ($jp_industry_id_c==$i->industry_id)?TRUE:FALSE); ?>><?php echo $i->industry_name?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <?php echo form_error('jp_industry_id_c'); ?>
                </li>
                <li class="form-field jp_expected_salary">
                    <label for="jp_expected_salary">Expected Salary</label>
                    <div class="input">
                        <input type="text" name="jp_expected_salary" id="jp_expected_salary" value="<?php echo set_value('jp_expected_salary',$jp_expected_salary);?>" />
                    </div>
                    <?php echo form_error('jp_expected_salary'); ?>
                </li>
                <li class="form-field jp_start_date">
                    <label for="jp_start_date">Date Available</label>
                    <div class="input">
                        <input type="text" name="jp_start_date" id="jp_start_date" value="<?php echo set_value('jp_start_date',$jp_start_date);?>" class="date-picker" />
                    </div>
                    <?php echo form_error('jp_start_date'); ?>
                </li>
            </ul>
            <div class="action-set">
                <input type="submit" value="Submit" />
            </div>
        </form>
    </div>
    <div class="right" style="width:280px">
        
        <h3 style="margin-top:0">Edit Account</h3>
        <form method="post" action="<?php echo site_url('profile/change_email');?>" class="ajax">
            <span>Change Email Address</span><br />
            <input type="text" name="email_address" value="<?php echo $email_address?>" />
            <input type="submit" value="Submit" /><br />
            <div style="line-height:1.5em; color:green"><em><strong>IMPORTANT:</strong> Changing your email address will log you out. You will receive a new activation code in your new email. Use it to re-activate your account.</em></div>
        </form>
        <br />
        <form method="post" action="<?php echo site_url('profile/change_password');?>" class="ajax">
            <label>Change Password</label>
            <div class="input">
                <input type="password" name="password" id="password" placeholder="Old Password" /><br />
                <input type="password" name="password2" id="password2" placeholder="New Password" style="margin-top:10px;" /><br />
                <input type="password" name="password3" id="password3" placeholder="Confirm New Password" />
                <input type="submit" value="Submit" />
            </div>
        </form>
        
    </div>

    <div class="clear"></div>
    
<style type="text/css">
    .field-set {
        padding:0;
        margin:0;
        list-style-type:none;
    }
    .field-set, .action-set {
        margin-top:20px;
    }

    .field-set .form-field {
        margin-top:10px;
    }
    .field-set .form-field:first-child {
        margin-top:0;
    }
    .field-set .form-field:after {
        content: " ";
        display:block;
        clear:both;
    }
    .field-set label {
        float:left;
        min-width:160px;
        font-size:1.1em;
    }
    .field-set .input {
        float:left;
    }
    .field-set .error {
        float:left;
        margin-left:5px;
    }
    .field-set .form-field .long {
        width:320px;
    }
    .field-set .form-field.required label:after {
        content: " *";
        color:#c00;
    }
</style>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.datepicker.setDefaults({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            });
            jQuery('.date-picker').datepicker();
            jQuery('form.ajax').ajaxForm({
                dataType: 'json',
                success: function(response,status,xhr,form){
                    msgprompt.html(response.message);
                    msgprompt.window('open');
                    form.resetForm();
                    if( response.redirect != '' && typeof response.redirect != "undefined") {
                        setTimeout(function(){
                            window.location = response.redirect;
                        },3000);
                    }
                }
            });
            
        });
    </script>
<?php $this->load->view('_blocks/footer') ?>
