<?php 

// declared here so we don't have to in each controller's variable file
$CI =& get_instance();

// generic global page variables used for all pages
$vars = array();
$vars['layout'] = 'main';
$vars['page_title'] = fuel_nav(array('render_type' => 'page_title', 'delimiter' => ' : ', 'order' => 'desc', 'home_link' => 'Home'));
$vars['meta_keywords'] = '';
$vars['meta_description'] = '';
$vars['js'] = array();
$vars['js'][] = 'jquery.form.min';
$vars['css'] = array('superfish/superfish');
$vars['css'][] = 'easyui/themes/icon';
$vars['css'][] = 'easyui/themes/default/easyui';
$vars['js'][] = 'easyui/jquery.easyui.min';

$vars['has_banner'] = 'no';

$uri_seg = $CI->uri->uri_string();
if (empty($uri_seg)) {
   $uri_seg = 'prime-man-power/home';
}
/* Page model */
$page = fuel_model('pages',
    array(
        'find' => 'one',
        'where'=>array('published' => 'yes', 'location'=> $uri_seg)
    )
);

$has_banner = (isset($page->has_banner)) ? $page->has_banner :
        $vars['has_banner'];

$page_id = (isset($page->id)) ? $page->id : 0;

if ($page_id > 0) {
    $page_var = fuel_model('pagevariables',
        array(
            'find' => 'one',
            'where'=>array('page_id' => $page_id, 'name'=> 'job_search_main')
        )
    );
}

$job_search_main = (isset($page_var->value)) ? $page_var->value :
        'no';

// page specific variables
if ($has_banner == 'yes') {
    $vars['css'][] = 'nivo-slider/themes/default/default';
    $vars['css'][] = 'nivo-slider/nivo-slider';
    $vars['js'][] = 'nivo-slider/jquery.nivo.slider';
    $vars['has_banner'] = $has_banner;
}

if (isset($job_search_main) && $job_search_main == 'yes') {
    
}

//$pages['about/(:any)'] = array(
//    'css' => $vars['css'],
//    'js' => $vars['js']
//);

?>