        </div>
    </div>
</div>

<div id="footerFull">
    <div id="footer">
        <?php echo fuel_nav(array('container_tag_id' => 'footer_link',
            'item_id_prefix' => 'footer_link_',
            'container_tag_class' => '',
            'group_id' => 'footer',
        )); ?>
        <br />
        © <?php echo date('Y') ?> Prime Manpower - All rights reserved
    </div>
</div>
<?php echo js('main').js($js); ?>

</body>
</html>