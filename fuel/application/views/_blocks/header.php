<!DOCTYPE html>

<html lang="en-US">
<head>
	<base href="<?php echo site_url()?>">
	<meta charset="utf-8">
	<title>
            <?php
                echo fuel_var('page_title', '');
            ?>
	</title>
	<meta name="google-site-verification" content="ohFZ2omxeORKwL7K3l52b84fF-WD8pt6d4UrnE7jjdI" />
	<meta name="keywords" content="<?php echo fuel_var('meta_keywords')?>">
	<meta name="description" content="<?php echo fuel_var('meta_description')?>">
	<?php if (isset($news_and_article)) { ?>
	<meta property="og:url" content="<?php echo $og_url; ?>" /> 
	<meta property="og:title" content="<?php echo strip_tags($og_title); ?>" />
	<meta property="og:description" content="<?php echo strip_tags($og_content); ?>" />
	<meta property="og:image" content="http://photos.prnewswire.com/prnvar/20110330/CG73938LOGO-a" /> 
	<?php } ?>
	<?php if (!empty($meta_robot_follow) && $meta_robot_follow == 'yes') { ?>
	<meta name="robots" content="nofollow" />
	<?php } ?>

	<?php
            echo css('style').css($css);
	?>
	
	<?php echo js('jquery.form.min'); ?>
	
	<?php echo jquery('1.8.3'); ?>
        <?php //echo js('prettyphoto/jquery-1.6.1.min').js($js); ?>
        <?php echo js('superfish/superfish').js($js); ?>
        <?php //Google anayltics here ?>
        <?php echo fuel_var('Google Analytics'); ?>
	<?php echo js('jqueryui-1.11.4/jquery-ui.min.js'); ?>
	<link href="<?php echo base_url("assets/js/jqueryui-1.11.4/jquery-ui.min.css"); ?>" media="all" rel="stylesheet"/>
	<link href="<?php echo base_url("assets/js/jqueryui-1.11.4/jquery-ui.structure.min.css"); ?>" media="all" rel="stylesheet"/>
	<link href="<?php echo base_url("assets/js/jqueryui-1.11.4/jquery-ui.theme.min.css"); ?>" media="all" rel="stylesheet"/>

</head>

<body class="<?php echo fuel_var('body_class', ''); ?>">

<div id="other_topnav">
    <div id="other_topnav_inner">
        <div id="other_topnav_inner_left_block">
            <?php echo fuel_nav(array('container_tag_id' => 'other_topnav_menu',
                    'item_id_prefix' => 'other_topnav_menu_',
                    'container_tag_class' => '',
                    'group_id' => 'other_topnav',
            )); ?>
        </div>
        <div id="other_topnav_inner_right_block">
            <div style="float:right">
                <form id="search_top_form" method="POST" action="<?php echo site_url('main/search_top') ?>">
                    <?php $q_var = (isset($q)) ? $q : ''; ?>
                    <label>Search: </label><input class="easyui-tooltip" title="Job title Or Position" type ="text" id="search_top" name="search_top" value ="<?php //echo $q_var; ?>"/>
                    <a style="cursor:pointer" id="search_anchor">
                        <img style="width: 18px;height: 15px;" src="<?php echo img_path('search.png'); ?>" />
                    </a>
                </form>
                <script>
                    $('#search_anchor').click(function(e){
                        e.preventDefault();
                        $('#search_top_form').submit();
                    })
                </script>
           </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="clear"></div>

<div id ="logo">
    <div style="float:left;width: 641px">
        <?php echo fuel_block('logo'); ?>
    </div>

    <div class="social_icons" style="float:right;position: relative;top: 10px;height: 0px;">
        <div>

            <div class="user-tools" style="text-align:right">
                <?php if( ! $this->fuel->auth->is_super_admin() ):?>
                    <?php if( ! $this->session->userdata('is_logged_in') ):?>
                    <a href="<?php echo site_url('profile/register');?>" style="color:#0000CD">Register</a>
                    <span style="color:#ddd">|</span>
                    <a href="<?php echo site_url('profile/login');?>" style="color:#0000CD">Login</a>
                    <?php else: ?>
                    <a href="<?php echo site_url('profile/logout');?>" style="color:#0000CD">Logout</a>
                    <?php endif;?>
                <?php else: ?>
                    &nbsp;<!-- spacer -->
                <?php endif;?>
            </div>
            <div class="social-tools" style="margin-top:15px;">
                <?php
                //$slides = fuel_model('slide_shows', array('order' => 'precedence ASC'));
                $tools = fuel_model('social_tools', array('return_method' => 'object'));
                foreach ($tools as $t) { ?>
                <a target="_blank" href="<?php echo $t->link; ?>">
                    <img style="width: 20px;height: 20px;" src="<?php echo img_path($t->image); ?>" />
                </a>
                <?php } ?>
            </div>
        </div>
        <div style="float:right;margin-top: 20px;" class="hide">
                <a href="mailto:hr@manpower.com<?php //echo //$manpower_email; ?>">
                    <img style="width: 20px;height: 20px;" src="<?php echo img_path('mail.png'); ?>" />
                </a>
                <a href="www.facebook.com/ManpowerPhilippines" target="_blank">
                  <img style="width: 20px;height: 20px;" src="<?php echo img_path('facebook_icon_small.png'); ?>" />
                </a>
                <a href="www.twitter.com/ManpowerPH" class="" data-show-count="false">
                    <img style="width: 20px;height: 20px;" src="<?php echo img_path('Twitter-icon.png'); ?>" />
                </a>
		<a href="http://www.linkedin.com/company/537101?trk=tyah&trkInfo=tas%3Amanpower%20ph" target="_blank">
		     <img style="width: 20px;height: 20px;" src="<?php echo img_path('linkedin-logo-icon.png'); ?>" />
                </a>
		<a href="http://bit.ly/linkManpowerPH" target="_blank">
		     <img style="width: 20px;height: 20px;" src="<?php echo img_path('Google-plus-icon.png'); ?>" />
                </a>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>

<div id="topnav">
    <div id ="topnav_inner">
        <?php echo fuel_nav(array('container_tag_id' => 'main_menu',
            'item_id_prefix' => 'topmenu_',
            'container_tag_class' => 'sf-menu',
            'exclude' => array('published' => 'yes')
        )); ?>
        <?php if( ! $this->fuel->auth->is_super_admin() ):?>
        <ul class="sf-menu">
            <li><a href="<?php echo site_url('profile');?>">My Prime Profile</a>
                <?php if( $this->session->userdata('is_logged_in') ):?>
                <ul>
                    <li><a href="<?php echo site_url('profile/edit');?>">Edit</a></li>
                    <li><a href="<?php echo site_url('profile/logout');?>">Logout</a></li>
                </ul>
                <?php endif;?>
            </li>
        </ul>
        <?php endif;?>
        <div class="clear"></div>
    </div>
</div>

<div id ="parent-container">
    <div id="container">
        <?php
//            echo fuel_nav(array(
//            'render_type' => 'breadcrumb',
//            'active' => $this->uri->uri_string(),
//            'container_tag_id' => 'breadcrumb',
//            'container_tag' => 'ul',
//            'item_tag' => 'li',
//        ));
        ?>
        <div style="height: 27px;"></div>
        <div id="main">

