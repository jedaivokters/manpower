<?php foreach($experiences as $exp):?>
<tr data-id="<?php echo $exp->id?>">
	<td><?php echo $exp->job_title?></td>
	<td><?php echo $exp->company?></td>
	<td><?php echo $exp->industry?></td>
	<td><?php echo $exp->contact_no?></td>
	<td><?php echo date('M d, Y',strtotime($exp->start_date)). ' - ' . date('M d, Y',strtotime($exp->end_date))?></td>
	<td>
		<a href="<?php echo site_url('profile/delete_experience/'.$exp->id)?>" class="btn-tool delete">Delete</a>
		<a href="#" class="btn-tool edit edit-exp trigger-dialog"
		   data-experience_id="<?php echo $exp->id?>"
		   data-job_title="<?php echo $exp->job_title?>"
		   data-company="<?php echo $exp->company?>"
		   data-location="<?php echo $exp->location?>"
		   data-industry="<?php echo $exp->industry?>"
		   data-contact_no="<?php echo $exp->contact_no?>"
		   data-start_date="<?php echo $exp->start_date?>"
		   data-end_date="<?php echo $exp->end_date?>"
		   data-job_description="<?php echo $exp->job_description?>"
		   data-dialog="add-exp-form" 
		   >Edit</a>
	</td>
</tr>
<?php endforeach;?>
