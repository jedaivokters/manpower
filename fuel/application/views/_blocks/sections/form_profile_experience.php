<input type="hidden" name="experience_id" value="" />
<div>
	<label>Job Title</label><br />
	<input type="text" name="job_title" class="validate" required />
</div>
<div>
	<label>Company</label><br />
	<input type="text" name="company" class="validate" required />
</div>
<div>
	<label>Location</label><br />
	<input type="text" name="location" class="validate" required />
</div>
<div>
	<label>Industry</label><br />
	<select name="industry">
		<?php if($industries):?>
		<?php foreach($industries as $i):?>
		<option value="<?php echo $i->industry_name?>"><?php echo $i->industry_name?></option>
		<?php endforeach;?>
		<?php endif;?>
	</select>
</div>
<div>
	<label>Contact No</label><br />
	<input type="text" name="contact_no" />
</div>
<div>
	<label>Start Date</label><br />
	<input type="text" name="start_date" class="date-picker" />
</div>
<div>
	<label>End Date</label><br />
	<input type="text" name="end_date" class="date-picker" />
</div>
<div>
	<label>Job Description</label><br />
	<textarea name="job_description" rows="10" cols="70"></textarea>
</div>
<div>
	<br />
	<input type="submit" value="Submit" />
	<input type="reset" value="Cancel" />
</div>