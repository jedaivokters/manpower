<input type="hidden" name="education_id" value="" />
<div>
	<label>Degree</label><br />
	<select name="degree_id" style="width:200px;">
		<option value="">- Select One -</option>
		<?php foreach($degrees as $c):?>
		<option value="<?php echo $c->degree_id?>"><?php echo $c->degree_title?></option>
		<?php endforeach;?>
		<option value="0">Other</option>
	</select>
</div>
<div style="display:none">
	<label>Degree (Others)</label><br />
	<input type="text" name="degree_others" />
</div>
<div>
	<label>School</label><br />
	<input type="text" name="school" class="validate" required />
</div>
<div>
	<label>Country</label><br />
	<select name="country_id">
		<option value="">- Select One -</option>
		<?php foreach($countries as $c):?>
		<option value="<?php echo $c->country_id?>"><?php echo $c->country_name?></option>
		<?php endforeach;?>
	</select>
</div>
<div>
	<label>City</label><br />
	<input type="text" name="city_name" />
	<select name="city_id" style="display:none">
		<option value="">- Select One -</option>
		<?php foreach($cities as $c):?>
		<option value="<?php echo $c->phil_city_id?>"><?php echo $c->phil_city_name?></option>
		<?php endforeach;?>
	</select>
</div>
<div>
	<label>Province</label><br />
	<input type="text" name="province_name" />
	<select name="province_id" style="display:none">
		<option value="">- Select One -</option>
		<?php foreach($provinces as $c):?>
		<option value="<?php echo $c->phil_province_id?>"><?php echo $c->phil_province_name?></option>
		<?php endforeach;?>
	</select>
</div>
<div>
	<label>Start Date</label><br />
	<input type="text" name="start_date" class="date-picker" />
</div>
<div>
	<label>End Date</label><br />
	<input type="text" name="end_date" class="date-picker" />
</div>
<div>
	<br />
	<input type="submit" value="Submit" />
	<input type="reset" value="Cancel" />
</div>