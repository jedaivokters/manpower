<div>
	<label>Skill Title</label><br />
	
	<select name="skill_id" class="validate" required>
		<?php foreach($skills_list as $s):?>
		
		<option value="<?php echo $s->skill_id?>"><?php echo $s->skill_name?></option>
		<?php endforeach;?>
		
	</select>
	<div style="display:none"><input type="text" name="title" /></div>
</div>
<div>
	<?php
		$enum = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "applicants_skills" AND COLUMN_NAME = "proficiency"')->result();
		$enum2 = $this->db->query('SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "applicants_skills" AND COLUMN_NAME = "yrs_exp"')->result();
		$list = explode(',',substr($enum[0]->COLUMN_TYPE,5,-1));
		$list2 = explode(',',substr($enum2[0]->COLUMN_TYPE,5,-1));
	?>
	<label>Proficiency</label><br />
	<select name="proficiency">
		<?php foreach($list as $l):?>
		<option value="<?php echo substr($l,'1','-1')?>"><?php echo substr($l,'1','-1')?></option>
		<?php endforeach;?>
	</select>
</div>
<div>
	<label>Years of Experience</label><br />
	
	<select name="yrs_exp">
            <?php foreach($list2 as $l):?>
            <option value="<?php echo substr($l,'1','-1')?>"><?php echo substr($l,'1','-1')?></option>
            <?php endforeach;?>
	</select>
</div>
<div>
	<br />
	<input type="submit" value="Submit" />
	<input type="reset" value="Cancel" />
</div>