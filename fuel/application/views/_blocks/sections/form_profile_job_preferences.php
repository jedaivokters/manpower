<form id="jp_form" name="jp_form" action ="<?php echo site_url('profile/apply/' . $j_id) ?>" method="POST" >
    <input type="hidden" name="bypass" value="T" />
    <div>
        <label>Position Type</label><br />

        <select name="jp_position_type">
            <?php foreach ($position_types as $p): ?>
                <option value="<?php echo substr($p, '1', '-1'); ?>"><?php echo substr($p, '1', '-1'); ?></option>
            <?php endforeach; ?>

            </select>
            <div style="display:none"><input type="text" name="title" /></div>
        </div>
        <div>
            <label>Preferred Location</label><br />
            <select name="jp_prefered_location">
            <?php foreach ($locations as $l): ?>
                    <option value="<?php echo substr($l, '1', '-1') ?>"><?php echo substr($l, '1', '-1') ?></option>
            <?php endforeach; ?>
                </select>
            </div>
            <div>
                <label>Areas Abroad</label><br />
                <input type="text" name="jp_prefered_areas_abroad" /> (type: N/A if not applicable)
            </div>
            <div>
                <label>Areas Local</label><br />
                <input type="text" name="jp_prefered_areas_local" /> (type: N/A if not applicable)
            </div>
            <div>
                <label>Relocate to Philippines?</label><br />
                <select name="jp_relocate_philippines">
            <?php foreach ($yesno as $y): ?>
                        <option value="<?php echo substr($y, '1', '-1') ?>"><?php echo substr($y, '1', '-1') ?></option>
            <?php endforeach; ?>
                    </select>
                </div>
                <div>
                    <label>Relocate to Abroad?</label><br />
                    <select name="jp_relocate_abroad">
            <?php foreach ($yesno as $y): ?>
                            <option value="<?php echo substr($y, '1', '-1') ?>"><?php echo substr($y, '1', '-1') ?></option>
            <?php endforeach; ?>
                        </select>
                    </div>
                    <div>
                        <label>1. Preferred Industry</label><br />
                        <select name="jp_industry_id_a">
            <?php foreach ($industries as $i): ?>
                                <option value="<?php echo $i->industry_id; ?>"><?php echo $i->industry_name; ?></option>
            <?php endforeach; ?>
                            </select>
                        </div>
                        <div>
                            <label>2. Preferred Industry</label><br />
                            <select name="jp_industry_id_b">
            <?php foreach ($industries as $i): ?>
                                    <option value="<?php echo $i->industry_id; ?>"><?php echo $i->industry_name; ?></option>
            <?php endforeach; ?>
                                </select>
                            </div>
                            <div>
                                <label>3. Preferred Industry</label><br />
                                <select name="jp_industry_id_c">
            <?php foreach ($industries as $i): ?>
                                        <option value="<?php echo $i->industry_id; ?>"><?php echo $i->industry_name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <label>Expected Salary</label><br />
        <input type="text" name="jp_expected_salary" />
    </div>
    <div>
        <label>Start Date</label><br />
        <input id="jp_start_date" type="text" name="jp_start_date" />
    </div>
    <div>
        <br />
        <input id="jp_submit" type="submit" value="Submit" />
        <input id="jp_cancel" type="reset" value="Cancel" />
    </div>
</form>
<script>
    $('.easyui-window').window('setTitle','Job Preferences');
    $('.easyui-window').window('move',{top: 0});
    $('#jp_start_date').datepicker();
    $('#jp_cancel').click(function(e){
        e.preventDefault();
        $('.easyui-window').window('close');
    });

    $('#jp_submit').click(function(e){
        e.preventDefault();
        
        var msgprompt = jQuery('<div class="easyui-window" style="min-width:300px;padding:20px" title="Message" />');
        jQuery('body').append(msgprompt);
        jQuery('.easyui-window').window({
            closed: true,
            modal: true,
            minimizable: false,
            maximizable: false,
            resizable: false,
            collapsible: false,
            closable: true
        });
        
        $.ajax({
            url: $('#jp_form').attr('action'),
            data: $('#jp_form').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function( xhr ) {
                $('.easyui-window').window('close');
            },
            success: function(response) {
                if(response.message != '') {
                    msgprompt.html(response.message);
                    msgprompt.window('open');
                }
            }
        });
    });
</script>