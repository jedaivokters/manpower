<?php foreach($educations as $e):?>
<tr data-id="<?php echo $e->id?>">
	<td>
		<?php if( $e->degree_id == 0 ):?>
			<?php echo $e->degree_others?>
		<?php else: ?>
			<?php echo $e->degree_title?>
		<?php endif;?>
	</td>
	<td><?php echo $e->school?></td>
	<td><?php echo $e->country_name?></td>
	<td><?php echo date('M d, Y',strtotime($e->start_date)). ' - ' . date('M d, Y',strtotime($e->end_date))?></td>
	<td>
		<a href="<?php echo site_url('profile/delete_education/'.$e->id)?>" class="btn-tool delete">Delete</a>
		<a href="#" class="btn-tool edit edit-edu trigger-dialog"
		   data-education_id="<?php echo $e->id?>"
		   data-degree_id="<?php echo $e->degree_id?>"
		   data-degree_others="<?php echo $e->degree_others?>"
		   data-school="<?php echo $e->school?>"
		   data-country_id="<?php echo $e->country_id?>"
		   data-city_id="<?php echo $e->city_id?>"
		   data-city_name="<?php echo $e->city_name?>"
		   data-province_id="<?php echo $e->province_id?>"
		   data-province_name="<?php echo $e->province_name?>"
		   data-start_date="<?php echo $e->start_date?>"
		   data-end_date="<?php echo $e->end_date?>"
		   data-dialog="add-edu-form"
		   >Edit</a>
	</td>
</tr>
<?php endforeach;?>
