<?php 
$route[FUEL_FOLDER.'/job/dashboard'] = JOB_FOLDER.'/dashboard';
$route[FUEL_FOLDER.'/job/dashboard/applicants/(:any)'] = JOB_FOLDER.'/dashboard/applicants';
$route[FUEL_FOLDER.'/job/dashboard/applicant/(:any)'] = JOB_FOLDER.'/dashboard/applicant';
$route[FUEL_FOLDER.'/job/dashboard/download/(:any)'] = JOB_FOLDER.'/dashboard/download';
$route[FUEL_FOLDER.'/job/search'] = JOB_FOLDER.'/search';
$route[FUEL_FOLDER.'/job/applicants_search'] = JOB_FOLDER.'/search/applicants_search';


