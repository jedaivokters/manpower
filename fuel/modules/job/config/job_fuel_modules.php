<?php
$config['modules']['job'] = array(
	'module_name' => 'Job Dashboard',
	'module_uri' => 'job/dashboard',
	'model_name' => 'job_posts_model',
	'model_location' => '',
	'permission' => 'job',
);