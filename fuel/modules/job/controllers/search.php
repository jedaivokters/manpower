<?php
require_once(FUEL_PATH.'/libraries/Fuel_base_controller.php');

class Search extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->config('job');
		$config = $this->config->item('job');
                $this->load->module_model(FUEL_FOLDER, 'job_posts_model');
                $this->load->model('job_applicants_model');
	}
	
	function index()
	{
            //Pagination
            $page = $this->input->post('page', TRUE);
            $rows = $this->input->post('rows', TRUE);
            //Filters
            $keywords = $this->input->post('keywords', TRUE);
            //$location = $this->input->post('location', TRUE);
            $search_type = $this->input->post('search_type', TRUE);
            //If in Job dashboard
            $query_id = $this->input->post('query_id', TRUE);
            //Sort
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

            $page = isset($_POST['page']) ?
                    intval($_POST['page']) : 1;
            $rows = isset($_POST['rows'])
                    ? intval($_POST['rows']) : 10;

            //10 rows?
            //$rows = 10;

            $where = 'job_posts.published = "yes" AND ("'.date('Y-m-d').'" BETWEEN from_date AND to_date OR ISNULL(from_date) AND ISNULL(to_date))';
            $where .= ($search_type == 'all') ? '' : ' AND p.search_type ="'.$search_type.'"';
            $where .= (empty($query_id) ) ? '' : ' AND job_posts.creator_id ="'.$query_id.'"';

            if (! empty($keywords) ) {
                //$conjunction = (! empty($location) ) ? ' AND ' : '';
                $where .= ' AND (job_posts.title LIKE "%'.$keywords.'%" 
                    OR p.name LIKE "%'.$keywords.'%" OR job_posts.zipcode LIKE "%'.$keywords.'%"
                    OR job_posts.city LIKE "%'.$keywords.'%")';
            }
//
//            if (! empty($location) ) {
//                $conjunction = (empty($keywords) ) ? ' AND ' : '';
//                $where .= $conjunction.'(job_posts.zipcode LIKE "%'.$location.'%"
//                    OR job_posts.city LIKE "%'.$location.'%")';
//            }

            $params['select'] = 'job_posts.id, job_posts.content, job_posts.title job_title,
                job_posts.city, ot.name occupation_type_name, p.name position, 0 action';
            $params['where'] = $where;
            $params['join'] = array(
                array('occupation_type ot','job_posts.occupation_type_id = ot.id'),
                array('positions p','p.id = job_posts.position_type_id')
            );
            $params['order_by'] = $sort.' '.$order;

            //Query to get total no. of records
            $job_posts = $this->job_posts_model->query($params);
            $total_record = count($job_posts->result());

            //Include offset for actual listing
            $params['limit'] = $rows;
            $params['offset'] = $page - 1;

            $job_posts = $this->job_posts_model->query($params);

            //$total_count = $this->job_posts_model->total_record_count();
//            $total_count = $job_posts->num_rows();

            $items = array();
            
            foreach ($job_posts->result() as $row)
            {
               array_push($items, array(
                'id' => $row->id,
                'job_title' => $row->job_title,
                'city' => $row->city,
                'occupation_type' => $row->occupation_type_name,
                'content' => strip_tags($row->content),
                'position' => $row->position,
                'action' => $row->action,
               ));
            }

            $result["rows"] = $items;
            $result["total"] = $total_record;
            //print_r($result);
            echo json_encode($result);
			return;
            
            if (isset($_POST['query_id'])) {
                echo json_encode($result);
                return;
            }

            $result['page'] = $page;
            $result['pages'] = ceil($total_record / $rows);
            $result['job_search_type'] = $search_type;
            

            //$this->fuel->pages->render('job_lists', $result, array('view_module' => 'job'));
	}
	function applicants_search()
	{
            //Pagination
            $page = $this->input->post('page', TRUE);
            $rows = $this->input->post('rows', TRUE);
            //Filters
            $keywords = $this->input->post('keywords', TRUE);
            //$location = $this->input->post('location', TRUE);
            //If in Job dashboard
            $job_id = $this->input->post('job_id', TRUE);
            //Sort
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

            $page = isset($page) ?
                    intval($page) : 1;
            $rows = isset($rows)
                    ? intval($rows) : 10;

            $where = 'job_post_id ="'.$job_id.'"';

            if (! empty($keywords) ) {
                //$conjunction = (! empty($location) ) ? ' AND ' : '';
                $where .= ' AND (firstname LIKE "%'.$keywords.'%"
                    OR lastname LIKE "%'.$keywords.'%")';
            }
//
//            if (! empty($location) ) {
//                $conjunction = (empty($keywords) ) ? ' AND ' : '';
//                $where .= $conjunction.'(job_posts.zipcode LIKE "%'.$location.'%"
//                    OR job_posts.city LIKE "%'.$location.'%")';
//            }

            $params['select'] = 'id, firstname, lastname, email_address,
                province, contact_no, 0 action';
            $params['where'] = $where;
            /*$params['join'] = array(
                array('occupation_type ot','job_posts.occupation_type_id = ot.id'),
                array('positions p','p.id = job_posts.position_type_id')
            );*/
            $params['order_by'] = $sort.' '.$order;

            //Query to get total no. of records
            $applicants = $this->job_applicants_model->query($params);
            $total_record = count($applicants->result());

            //Include offset for actual listing
            $params['limit'] = $rows;
            $params['offset'] = $page - 1;

            $applicants = $this->job_applicants_model->query($params);

            //$total_count = $this->job_posts_model->total_record_count();
//            $total_count = $job_posts->num_rows();

            $items = array();

            foreach ($applicants->result() as $row)
            {
               array_push($items, array(
                'id' => $row->id,
                'firstname' => $row->firstname,
                'lastname' => $row->lastname,
                'email_address' => $row->email_address,
                'province' => $row->province,
                'contact_no' => $row->contact_no,
                'action' => $row->action,
               ));
            }

            $result["rows"] = $items;
            $result["total"] = $total_record;
            //print_r($result);
            echo json_encode($result);
	}

}