<?php
require_once(FUEL_PATH.'/libraries/Fuel_base_controller.php');

class Dashboard extends Fuel_base_controller {
	
	function __construct()
	{
		parent::__construct();
		$this->_validate_user('job', NULL, FALSE);
                //Load Models
                $this->load->module_model(FUEL_FOLDER, 'job_posts_model');
                $this->load->model('job_applicants_model');
	}
	
	function index()
	{
            $rec = $this->fuel->auth->valid_user();
            
            $data['vars']['css'] = '';
            $data['query_id'] = $rec['id'];
            
            $this->fuel->pages->render('main', $data, array('view_module' => 'job'));
		
	}

	function applicants($job_id = 0)
	{
            $rec = $this->fuel->auth->valid_user();

            $job_post = $this->job_posts_model->find_one(array(
                'published' => 'yes',
                'id' => $job_id,
            ));

            $data['vars']['css'] = '';
            $data['job_id'] = $job_id;
            $data['job'] = $job_post;

            $this->fuel->pages->render('applicants', $data, array('view_module' => 'job'));

	}

	function applicant($applicant_id = 0)
	{
            $rec = $this->fuel->auth->valid_user();

            $applicant = $this->job_applicants_model->find_one(array(
                'id' => $applicant_id,
            ));

            $job_post = $this->job_posts_model->find_one(array(
                'published' => 'yes',
                'id' => $applicant->job_post_id,
            ));

            $data['vars']['css'] = '';
            $data['job_id'] = $applicant->job_post_id;
            $data['applicant'] = $applicant;
            $data['job'] = $job_post;

            $this->fuel->pages->render('applicant_detail', $data, array('view_module' => 'job'));

	}
	function download($applicant_id = 0)
	{
            $this->load->helper('download');
            $rec = $this->fuel->auth->valid_user();

            $applicant = $this->job_applicants_model->find_one(array(
                'id' => $applicant_id,
            ));

            if ( ! file_exists(WEB_ROOT.'fuel/resumes/'.$applicant->attachfile)) {
                show_404();
                return;
            }

            $data = file_get_contents(WEB_ROOT.'fuel/resumes/'.$applicant->attachfile); // Read the file's contents
            $name = $applicant->firstname.'_'.$applicant->attachfile;

            force_download($name, $data);
	}

}