<?php
$vars['css'][] = 'easyui/themes/icon';
$vars['css'][] = 'easyui/themes/default/easyui';
$vars['js'][] = 'easyui/jquery.easyui.min';

echo fuel_block(array('view' => 'header', 'module' => 'job'), $vars, FALSE); ?>

<div class="main_inner">
    <h2>Applicant for - <?php echo $job->title; ?></h2>
    <br />

    <div id="apply_panel">
        <div title="Apply Now!" style="padding:10px;">
            <form id="apply_form" class="form" action ="<?php echo site_url('main/apply') ?>" enctype='multipart/form-data'>
                <div class="left" style="width: 428px;">
                    <div>
                        <label>
                            First Name:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->firstname; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>Middle Name:</label>
                        <div class="left" >
                            <strong><?php echo $applicant->middlename; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>
                            Last Name:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->lastname; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>
                            E-mail:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->email_address; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <!-- Column 2 -->
                <div class="left" style="width: 470px;">
                    <div>
                        <label>
                            Street Address:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->street_address; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>Street Address #2:</label>
                        <div class="left" >
                            <strong><?php echo $applicant->street_address2; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>
                            City:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->city; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>
                            Province:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->province; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>
                            Zip Code:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->zip_code; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>
                            Contact No:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->contact_no; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label>
                            <span class="required"></span>Contact No #2:
                        </label>
                        <div class="left">
                            <strong><?php echo $applicant->contact_no2; ?></strong>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="clear"></div>
                <br />
                <a id="apply_form_submit_btn" href="<?php echo site_url(JOB_FOLDER.'/dashboard/download/'.$applicant->id) ?>" class="easyui-linkbutton">Download Attachment</a>
            </form>
        </div>
    </div>

    <script>
        
    </script>
    <div class="clear"></div>
</div>

<?php echo fuel_block(array('view' => 'footer', 'module' => 'job'), array(), FALSE) ?>
