<?php foreach($rows as $r) { ?>
<div class="main_inner" style="width: auto">
    <h3><a target="" title="" href="<?php echo site_url('main/details/'. $r['id']); ?>" ><?php echo $r['job_title']; ?></a></h3>

    <?php echo word_limiter($r['content'], 50); ?>&nbsp;<a target="" title="" href="<?php echo site_url('main/details/'. $r['id']); ?>">Read More&nbsp >></a>
</div>
<br />
<?php } ?>
<?php if ($total > 10) { ?>
<div class="row">
    <div class="left" id="job_list_arrow_prev" style="cursor: pointer;border: 1px #0000CD solid;width: 20px;height: 20px;background-color: #207bc0; color: white; font-weight: bold;padding-left: 7px;margin-right: 2px;"><</div>
    <div class="left" id="job_list_arrow_next" style="cursor: pointer;border: 1px #0000CD solid;width: 20px;height: 20px;background-color: #207bc0; color: white; font-weight: bold;padding-left: 7px;">></div>
    <div class="clear">Page <?php echo $page; ?> out of <?php echo $pages; ?></div>
</div>
<?php } ?>
<script>
    var page_me = '<?php echo $page; ?>';
    var pages_me = '<?php echo $pages; ?>';

    $('#job_list_arrow_prev').click(function(){
        if (page_me != 1) {
            var search = {
                keywords: $('#keywords').val(),
                search_type: '<?php echo $job_search_type; ?>',
                page: parseInt(page_me) - 1
            }

            $('#job_container').load(job_search_url, search);
        }
    });

    $('#job_list_arrow_next').click(function(){
        if (page_me != pages_me) {
            var search = {
                keywords: $('#keywords').val(),
                search_type: '<?php echo $job_search_type; ?>',
                page: parseInt(page_me) + 1
            }

            $('#job_container').load(job_search_url, search);
        }
    });
</script>

