<?php
$vars['css'][] = 'easyui/themes/icon';
$vars['css'][] = 'easyui/themes/default/easyui';
$vars['js'][] = 'easyui/jquery.easyui.min';

echo fuel_block(array('view' => 'header', 'module' => 'job'), $vars, FALSE); ?>
            <div id="job_search_main">
                <h2>Applicants for Job - <?php echo $job->title; ?></h2>
                <div class="row">
                    <form id="search_form_advanced">
                        <label>Keywords</label>
                        <input type="text" name="keywords" id ="keywords" title="Name" class="easyui-tooltip" />
                        <a href="#" id="applicant_search_btn" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
                        <input type="submit" style="position:absolute;top:-100px"/>
                    </form>
                </div>
                <br />
                <table id="applicants_table" title="Search" style="height:320px"></table>
                
                <script>
                    var applicant_url = '<?php echo site_url('job/dashboard/applicant'); ?>';
                    var applicant_search_url =  '<?php echo site_url('fuel/job/applicants_search'); ?>';

                     $('#applicant_search_btn').click(function(e){
                        e.preventDefault();
                       $('#applicants_table').datagrid('load', {
                            keywords: $('#keywords').val(),
                            job_id: '<?php echo $job_id ?>'
                        });
                    });


                    $('#applicants_table').datagrid({
                        queryParams: {
                            keywords: $('#keywords').val(),
                            job_id: '<?php echo $job_id ?>'
                        },
                        noheader: true,
                        url: applicant_search_url,
                        idField:'id',
                        pagination: true,
                        method: 'post',
                        singleSelect: true,
                        columns:[[
                            {field:'id',title:'Code',width:150, hidden: true},
                            {field:'firstname',title:'First Name',width:150, sortable: true},
                            {field:'lastname',title:'Last Name',width:100,align:'left'},
                            {field:'email_address',title:'E-mail',width:150,align:'center'},
                            {field:'province',title:'Province',width:150,align:'center'},
                            {field:'contact_no',title:'Contact No',width:190,align:'center'},
                            {field:'action',title:'Action',width:130,align:'center', formatter: function(value,row,index){
				return '<a target="_blank" href="'+applicant_url+ '/'
                                    + row.id +'" class="apply_btn">View Details</a>';
                            }}
                        ]],
                        onLoadSuccess: function(data) {
                            $('.apply_btn').linkbutton();
                        }
                    });

                    
                </script>
            </div>

<?php echo fuel_block(array('view' => 'footer', 'module' => 'job'), array(), FALSE) ?>
