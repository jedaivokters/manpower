<!DOCTYPE html>

<html lang="en-US">
<head>
	<meta charset="utf-8">
	<title>
            <?php
                echo fuel_var('page_title', '');
            ?>
	</title>

	<meta name="keywords" content="<?php echo fuel_var('meta_keywords')?>">
	<meta name="description" content="<?php echo fuel_var('meta_description')?>">

	<?php
            echo css('style_job').css($css);
            echo css('superfish/superfish_job').css($css);
	?>

	<?php echo jquery('1.8.3'); ?>
        <?php echo js('superfish/superfish').js($js); ?>

</head>

<body class="<?php echo fuel_var('body_class', ''); ?>">

<div class="clear"></div>

<div id ="logo">
    <div style="float:left;width: 641px">
        <?php echo fuel_block('logo'); ?>
    </div>
    <div class="social_icons" style="width: 319px;float:left;"></div>
    <div class="clear"></div>
</div>

<div id="topnav">
    <div id ="topnav_inner">
        <?php echo fuel_nav(array('container_tag_id' => 'job_top_menu',
            'item_id_prefix' => 'topmenu_',
            'container_tag_class' => 'sf-menu',
            'group_id' => 'job_top_menu',
            'exclude' => array('published' => 'yes')
        )); ?>
        <div class="clear"></div>
    </div>
</div>

<div id ="parent-container">
    <div id="container">
        <div style="height: 27px;"></div>
        <div id="main">
