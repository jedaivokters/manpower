<?php
$vars['css'][] = 'easyui/themes/icon';
$vars['css'][] = 'easyui/themes/default/easyui';
$vars['js'][] = 'easyui/jquery.easyui.min';

echo fuel_block(array('view' => 'header', 'module' => 'job'), $vars, FALSE); ?>
            <div id="job_search_main">
                <h2>My Jobs</h2>
                <div class="row">
                    <form id="search_form_advanced">
                        <label>Keywords</label>
                        <input type="text" name="keywords" id ="keywords" title="Job Title Or Position" class="easyui-tooltip" />
                        <a href="#" id="job_search_btn" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
                        <input type="submit" style="position:absolute;top:-100px"/>
                    </form>
                </div>
                <br />
                <table id="job_table" title="Search" style="height:320px"></table>
                
                <script>
                    var applicants_url = '<?php echo site_url('job/dashboard/applicants'); ?>';
                    var job_search_url =  '<?php echo site_url('job/search'); ?>';

                     $('#job_search_btn').click(function(e){
                        e.preventDefault();
                       $('#job_table').datagrid('load', {
                            keywords: $('#keywords').val(),
                            search_type: 'all',
                            query_id: '<?php echo $query_id ?>'
                        });
                    });


                    $('#job_table').datagrid({
                        queryParams: {
                            keywords: $('#keywords').val(),
                            search_type: 'all',
                            query_id: '<?php echo $query_id ?>'
                        },
                        noheader: true,
                        url: job_search_url,
                        idField:'id',
                        pagination: true,
                        method: 'post',
                        singleSelect: true,
                        columns:[[
                            {field:'id',title:'Code',width:100, hidden: true},
                            {field:'job_title',title:'Job Title',width:350, sortable: true},
                            {field:'city',title:'Location',width:100,align:'right'},
                            {field:'position',title:'Position',width:100,align:'right'},
                            {field:'occupation_type',title:'Type',width:90,align:'right'},
                            {field:'action',title:'Action',width:230,align:'right', formatter: function(value,row,index){
				return '<a href="'+applicants_url+ '/'
                                    + row.id +'" class="apply_btn">View Applicants</a>';
                            }}
                        ]],
                        onLoadSuccess: function(data) {
                            $('.apply_btn').linkbutton();
                        }
                    });

                    
                </script>
            </div>
        


<?php echo fuel_block(array('view' => 'footer', 'module' => 'job'), array(), FALSE) ?>
