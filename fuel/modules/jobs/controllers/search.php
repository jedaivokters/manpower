<?php
require_once(FUEL_PATH.'/libraries/Fuel_base_controller.php');

class Search extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->config('job');
		$config = $this->config->item('job');
                $this->load->module_model(FUEL_FOLDER, 'job_posts_model');
	}
	
	function index()
	{
            //Pagination
            $page = $this->input->post('page', TRUE);
            $rows = $this->input->post('rows', TRUE);
            //Filters
            $keywords = $this->input->post('keywords', TRUE);
            $location = $this->input->post('location', TRUE);
            //Sort
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

            $page = isset($page) ?
                    intval($page) : 1;
            $rows = isset($rows)
                    ? intval($rows) : 10;

            $where = 'job_posts.published = "yes"';

            if (! empty($keywords) ) {
                $conjunction = (! empty($location) ) ? ' AND ' : '';
                $where .= ' AND job_posts.title LIKE "%'.$keywords.'%"
							OR p.name LIKE "%'.$keywords.'%"
							OR job_posts.content LIKE "%'.$keywords.'%"'
                    . $conjunction;
            }
            
            if (! empty($location) ) {
                $conjunction = (empty($keywords) ) ? ' AND ' : '';
                $where .= $conjunction.'(job_posts.zipcode LIKE "%'.$location.'%"
                    OR job_posts.city LIKE "%'.$location.'%")';
            }

            $params['select'] = 'job_posts.id, job_posts.content, job_posts.title job_title,
                job_posts.city, ot.name occupation_type_name, p.name position, 0 action';
            $params['where'] = $where;
            $params['join'] = array(
                array('occupation_type ot','job_posts.occupation_type_id = ot.id'),
                array('positions p','job_posts.position_type_id = p.id')
            );
            $params['order_by'] = $sort.' '.$order;
            $params['limit'] = $rows;
            $params['offset'] = $page - 1;

            $job_posts = $this->job_posts_model->query($params);
            //$total_count = $this->job_posts_model->total_record_count();
//            $total_count = $job_posts->num_rows();

            $items = array();
            foreach ($job_posts->result() as $row)
            {
               array_push($items, array(
                'id' => $row->id,
                'job_title' => $row->job_title,
                'city' => $row->city,
                'occupation_type' => $row->occupation_type_name,
                'content' => strip_tags($row->content),
                'position' => $row->position,
                'action' => $row->action,
               ));
            }

            $result["rows"] = $items;
            $result["total"] = $this->job_posts_model->record_count($where);
            //print_r($result);
            echo json_encode($result);
	}

}
