<?php
$config['nav']['clicky'] = array(
	'clicky/dashboard' => lang('module_blog_posts'),
	'blog/categories' => lang('module_blog_categories'),
	'blog/comments' => lang('module_blog_comments'),
	'blog/links' => lang('module_blog_links'),
	'blog/users' => lang('module_blog_authors'),
);

$config['clicky'] = array();
$config['clicky']['url'] = 'http://getclicky.com/user/login';
$config['clicky']['login'] = '';
$config['clicky']['pwd'] = '';
$config['clicky']['api'] = '';
$config['clicky']['permission'] = 'clicky';